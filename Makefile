### Settings

### Change to the desired install prefix. Make will install executables to $(prefix)/bin
# and libraries to $(prefix)/lib. If left commented, this will default to an in-source installation.
#INSTALL_PREFIX = 

### Uncomment to specify a directory to store computed files as necessary. If left commented
# this will default to $(INSTALL_PREFIX)/var
#VAR_DIR =

### Uncomment if you want to install a patch for COSMOMC which allows it to sample CosmoSuite's 
# likelihood function. This will also allow the "confgen" executable to install .ini files
# directly to this directory.
#COSMOMC_ROOT = 


### Don't change anything past here.



mkfile_path 	:= $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir 	:= $(shell dirname $(mkfile_path))
invocation_root := $(shell pwd)

INSTALL_PREFIX	?= $(current_dir)

DIR_BIN			?= $(INSTALL_PREFIX)/bin
DIR_LIB			?= $(INSTALL_PREFIX)/lib
DIR_DOC			?= $(INSTALL_PREFIX)/doc
DIR_VAR			?= $(current_dir)/var
DIR_CFG			?= $(DIR_VAR)/config

### All-target build flags
# 
CF_ALL			= -Wall -Werror \
		  		-funsigned-char \
		  		-Wextra -Wcast-qual -Wpointer-arith \
		  		-Wformat=2 -Wundef \
		  		-Wdisabled-optimization -Wshadow -Wmissing-braces \
		  		-Wstrict-aliasing=2 -Wconversion\
		  		-Wno-unused-parameter \
		  		-pedantic \
		  		-DDIR_VAR=\"$(DIR_VAR)\" \
		  		-DDIR_CFG=\"$(DIR_CFG)\"

ifneq (,$(COSMOMC_ROOT))				
CF_ALL			+= -DCOSMOMC_ROOT=\"$(COSMOMC_ROOT)\"
endif					

CF_ALL_INTEL	= -std=c99 -D_FORTIFY_SOURCE=2 

CF_ALL_GNU		= -Wcast-align -Wunreachable-code \
		  		-Wredundant-decls \
		  		-std=c11 \
				# -Wstrict-overflow=5

CF_DEBUG		= -O0 -g3 -DDEBUG

CF_RELEASE		= -O2 
CF_RELEASE_GNU	= -march=native -mtune=native -ftree-vectorize -D_FORTIFY_SOURCE=2 

LF_ALL			=
LL_ALL			= -lm -lgslcblas -lgsl -lcuba


### Build tools
# 
CC				?= icc
AR			 	= ar
RL			 	= ranlib
INST 		 	= ./.build/install
COMP		 	= $(CC) $(CF_ALL) $(CF_TGT) -o $@ -c $<
LINK		 	= $(CC) $(LF_ALL) $(LF_TGT) -o $@ $^ $(LL_TGT) $(LL_ALL)
COMPLINK	 	= $(CC) $(CF_ALL) $(CF_TGT) $(LF_ALL) $(LF_TGT) -o $@ $< $(LL_TGT) $(LL_ALL)
ARCH		 	= $(AR) rcs $@ $^
INDEX		 	= $(RL) $@



ifeq ($(CC),gcc)
CF_ALL 			+= $(CF_ALL_GNU)
CF_RELEASE 		+= $(CF_RELEASE_GNU)
else
CF_ALL 			+= $(CF_ALL_INTEL)
endif


include Rules.mk
