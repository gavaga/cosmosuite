# Standard things

.SUFFIXES:
.SUFFIXES:	.c .o

# default target is full-optimization
all: 		release

# debug target
debug: CF_ALL += $(CF_DEBUG)
debug: targets

# release target
release: CF_ALL += $(CF_RELEASE)
release: targets


# Subdirectories

dir		:= src
include		$(dir)/Rules.mk


# Directory-independent rules

%.o:		../%.c
		$(COMP)

%:		$(OBJDIR)/%.o
		$(LINK)

%:		%.c
		$(COMPLINK)

# Create .obj directory if needed
$(OBJDIR):
	mkdir -p $(OBJDIR)

$(DIR_VAR):
	mkdir -p $(DIR_VAR)

$(DIR_CFG):
	mkdir -p $(DIR_CFG)

$(DIR_BIN):
	mkdir -p $(DIR_BIN)

$(DIR_LIB):
	mkdir -p $(DIR_LIB)

$(DIR_DOC):
	mkdir -p $(DIR_DOC)

# The variables TGT_*, CLEAN and CMD_INST* may be added to by the Makefile
# fragments in the various subdirectories.

doc:		
ifeq (,$(shell which doxygen))
	$(error "Please make sure \"doxygen\" is visible in the PATH.")
else	
			cd $(current_dir)
			doxygen doc/Doxyfile
			cd $(invocation_root)
endif

targets:	$(TGT_BIN) $(TGT_LIB)

clean:		
		rm -f $(CLEAN)

install:	all | $(DIR_BIN) $(DIR_LIB) $(DIR_DOC)
		$(INST) $(TGT_BIN) -m 755 -d $(DIR_BIN)
		$(CMD_INSTBIN)
		$(INST) $(TGT_LIB) -m 750 -d $(DIR_LIB)
		$(CMD_INSTLIB)

# Prevent make from removing any build targets, including intermediate ones.

.SECONDARY:	$(CLEAN)

# Prevent problems if there are ever files with these names in the build

.PHONY:		debug release targets clean install doc
