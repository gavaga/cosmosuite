#!/usr/bin/python2
# -*- coding: utf-8 -*-

import sys, os, random, math
from PyQt5 import QtGui, QtCore

from StringIO import StringIO

import numpy as np
import scipy.stats

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QTAgg as NavigationToolbar
from matplotlib.patches import Ellipse
import matplotlib.pyplot as plt

from mpl_toolkits.mplot3d import Axes3D


qchar_Omega = QtCore.QChar(0x03a9)
qchar_beta = QtCore.QChar(0x03b2)
qchar_gamma = QtCore.QChar(0x03b3)
qchar_lambda = QtCore.QChar(0x03b)
qchar_nu = QtCore.QChar(0x03bd)
qchar_sigma = QtCore.QChar(0x03c3)

def load_array(filename):
    f = open(filename, 'r')

    a = f.read()
    f.close()

    dim1 = 0
    dim2 = 1

    data = ""
    if a[0] == '[':
        dim1 = int(a[1])
        if a[2] == ',':
            dim2 = int(a[3])
            data = a[5:]
        else:
            data = a[3:]
    else:
        data = a

    data_read = data.replace("),", "\n").replace("(", "").replace(")","")

    array = 0
    if data_read.find(",") > 0:
        array = np.genfromtxt(StringIO(data_read), delimiter=",") 
    else:
        array = np.genfromtxt(StringIO(data_read))

    return array



class PlotSettings():
    def __init__(self):
        self.types = [0,0]
        self.filenames = ["", "", "", ""]
        self.colors = [0x0000ff, 0xff0000]
        self.variable_names = ["", "", ""]
        self.indices = [0, 0, 0]
        self.draw_fid = False
        self.draw_max = False
        self.draw_eig = False
        self.log = False


class App(QtGui.QMainWindow):
    def __init__(self):
        super(App, self).__init__()

        self.plots = []
        self.layout = QtGui.QGridLayout()

        self.init_ui()
    
    def init_ui(self):
        self.statusBar().showMessage('Ready')

        exit_action = QtGui.QAction(QtGui.QIcon('../png/x.png'), '&Exit', self)
        exit_action.setShortcut('Ctrl+Q')
        exit_action.setStatusTip('Exit application')
        exit_action.triggered.connect(QtGui.qApp.quit)

        add_action = QtGui.QAction(QtGui.QIcon('../png/plus.png'), '&Add', self)
        add_action.setShortcut('Ctrl+N')
        add_action.setStatusTip('Add a new plot panel')
        add_action.triggered.connect(self.add_plot)

        clear_action = QtGui.QAction(QtGui.QIcon(''), '&Clear', self)
        clear_action.setShortcut('Ctrl+C')
        clear_action.setStatusTip('Clear all plots')
        clear_action.triggered.connect(self.remove_all)

    
        menubar = self.menuBar()
        file_menu = menubar.addMenu('&File')
        file_menu.addAction(exit_action)

        actions_menu = menubar.addMenu('&Actions')
        actions_menu.addAction(add_action)
        actions_menu.addAction(clear_action)

        central_widget = QtGui.QWidget()
        central_widget.setLayout(self.layout)
        self.setCentralWidget(central_widget)

        self.add_plot() 

        self.setWindowTitle('CosmoSuite Visualization Utility')
        self.show()

    def add_plot(self):
        self.statusBar().showMessage('Adding plot')
        self.plots.append(Plot())
        self.layout.addWidget(self.plots[len(self.plots) - 1], len(self.plots), 0)
        self.statusBar().showMessage('Done')

    def remove_all(self):
        for cnt in reversed(range(self.layout.count())):
            widget = self.layout.takeAt(cnt).widget()

            if widget is not None:
                widget.deleteLater()
        self.plots = []

class Plot(QtGui.QWidget):
    close_plot = QtCore.pyqtSignal()
    selected = QtCore.pyqtSignal()
    def __init__(self, parent=None):
        super(Plot, self).__init__(parent)

        self.init_ui()

    def init_ui(self):
        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)

        plot_action = QtGui.QAction(QtGui.QIcon('../png/graph-2x.png'), 'Plot', self)
        plot_action.triggered.connect(self.plot)

        exit_action = QtGui.QAction(QtGui.QIcon('../png/x-2x.png'), 'Exit', self)
        exit_action.triggered.connect(self.send_close_plot)

        settings_action = QtGui.QAction(QtGui.QIcon('../png/wrench-2x.png'), 'Settings', self)
        settings_action.triggered.connect(self.change_settings)

        self.spacer = QtGui.QWidget()
        self.spacer.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)

        self.utoolbar = QtGui.QToolBar('Utilities')
        self.utoolbar.addAction(plot_action)
        self.utoolbar.addAction(settings_action)
        self.utoolbar.addWidget(self.spacer)
        self.utoolbar.addAction(exit_action)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.utoolbar)
        layout.addWidget(self.canvas)
        layout.addWidget(self.toolbar)
        self.setLayout(layout)

    def plot(self):
        if self.settings.types[0] == 0:
            self.plot_cosmosurvey()
        elif self.settings.types[0] == 1:
            self.plot_heatmap()
        elif self.settings.types[0] == 2:
            self.plot_eig(True)

    def plot_cosmosurvey(self):
        plt.clf()
        ax = self.figure.add_subplot(111, projection='3d')

        cs_res = 0
        if self.settings.log:
            cs_res = np.log(load_array(self.settings.filenames[0])).tolist() #[[random.random() for x in xrange(10)] for x in xrange(10)]
        else:
            cs_res = load_array(self.settings.filenames[0]).tolist()

        vals = []
        for row in cs_res:
            vals = vals + row

        dz = 1
        dl = 1

        x = [i%len(cs_res[0]) for i in xrange(len(cs_res) * len(cs_res[0]))]
        y = [1*j for j in xrange(len(cs_res)) for i in xrange(len(cs_res[0]))]
        z = [0 for i in xrange(len(cs_res) * len(cs_res[0]))]

        ax.bar3d(x, y, z, dz, dl, vals)

        self.canvas.draw()

    def plot_heatmap(self):
        plt.clf()
        ax = self.figure.add_subplot(111)

        Z = 0

        if self.settings.log:
            Z = np.log(load_array(self.settings.filenames[0])).transpose()
        else:
            Z = load_array(self.settings.filenames[0]).transpose() 

        X,Y=np.meshgrid(range(Z.shape[0]+1),range(Z.shape[1]+1))
        im = plt.pcolormesh(X,Y,Z.transpose(), cmap='hot')
        plt.colorbar(im, orientation='horizontal')
        self.canvas.draw()

    def plot_eig(self, clear):
        if clear:
            plt.clf()

        fevc = self.settings.filenames[0]
        fevl = self.settings.filenames[1]
        fidfile = self.settings.filenames[2]
        ffish = self.settings.filenames[3]

        evc = load_array(fevc)
        evl = load_array(fevl)
        fid = load_array(fidfile)
        fish = load_array(ffish)

        cov = np.linalg.inv(fish) 

        i = self.settings.indices[0]
        j = self.settings.indices[1]

        xv = fid[i]
        yv = fid[j]

        ewidth = ((cov[i,i] + cov[j,j]) / 2)
        thing = (pow(cov[i,i] - cov[j,j], 2) / 4)
        thing = thing + pow(cov[i,j], 2)
        ewidth = ewidth + sqrt(thing)
        ewidth = sqrt(ewidth)
        eheight = sqrt( ((cov[i,i] + cov[j,j]) / 2) - sqrt( (pow(cov[i,i] - cov[j,j], 2) / 4) + pow(cov[i,j], 2) ) )
        eangle = 0.5 * atan((2 * cov[i,j]) / (cov[i,i] - cov[j,j]))

        el = Ellipse(xy=[xv, yv], width=ewidth, height=eheight, angle=eangle, fill=False, ec=self.settings.colors[0])
        
        ax = self.figure.add_subplot(111)

        ax.add_artist(el)
        self.canvas.draw()

    """

    def plot_histogram(self):
        plt.clf()
        ax = self.figure.add_subplot(111)


    def plot_chain_2d(self):

    def plot_chain_3d(self):
    """

    def change_settings(self):
        self.settings = SettingsDialog.get_settings()

    def mousePressEvent(self, e):
        self.selected.emit()

    def send_close_plot(self, e):
        self.deleteLater()

class SettingsDialog(QtGui.QDialog):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.settings = PlotSettings()
        self.init_ui()
           
    def init_ui(self):
        tabs = QtGui.QTabWidget()

        cs_tab = QtGui.QWidget()
        hm_tab = QtGui.QWidget()
        eig_tab = QtGui.QWidget()
        mcmc_tab = QtGui.QWidget()


        cs_layout = QtGui.QGridLayout()
        hm_layout = QtGui.QGridLayout()
        eig_layout = QtGui.QGridLayout()
        mcmc_layout = QtGui.QGridLayout()




        self.cs_cancel_button = QtGui.QPushButton('Cancel', self)
        self.cs_cancel_button.clicked.connect(self.deleteLater)

        self.cs_save_button = QtGui.QPushButton('Save', self)
        self.cs_save_button.clicked.connect(self.cs_save)
        self.cs_filename_line = QtGui.QLineEdit("CosmoSurvey output file...")

        if self.settings.filenames[0] != "":
            self.cs_filename_line.setText(self.settings.filenames[0])

        self.cs_file_button = QtGui.QPushButton('...', self)
        self.cs_file_button.setToolTip('Select a file')
        self.cs_file_button.resize(self.cs_file_button.sizeHint())
        self.cs_file_button.clicked.connect(self.cs_open_file)

        self.cs_log_button = QtGui.QCheckBox('Log Counts', self)

        cs_layout.addWidget(self.cs_filename_line, 0, 0, 1, 5)
        cs_layout.addWidget(self.cs_file_button, 0, 6)
        cs_layout.addWidget(self.cs_log_button, 1, 2, 1, 4)
        cs_layout.addWidget(self.cs_cancel_button, 2, 7)
        cs_layout.addWidget(self.cs_save_button, 2, 6)




        self.hm_cancel_button = QtGui.QPushButton('Cancel', self)
        self.hm_cancel_button.clicked.connect(self.deleteLater)

        self.hm_save_button = QtGui.QPushButton('Save', self)
        self.hm_save_button.clicked.connect(self.hm_save)
        self.hm_filename_line = QtGui.QLineEdit("Heatmap File...")

        if self.settings.filenames[0] != "":
            self.hm_filename_line.setText(self.settings.filenames[0])

        self.hm_file_button = QtGui.QPushButton('...', self)
        self.hm_file_button.setToolTip('Select a file')
        self.hm_file_button.resize(self.hm_file_button.sizeHint())
        self.hm_file_button.clicked.connect(self.hm_open_file)

        self.hm_log_button = QtGui.QCheckBox('Log Magnitudes', self)


        hm_layout.addWidget(self.hm_filename_line, 0, 0, 1, 5)
        hm_layout.addWidget(self.hm_file_button, 0, 6)
        hm_layout.addWidget(self.hm_log_button, 1, 2, 1, 4)
        hm_layout.addWidget(self.hm_cancel_button, 2, 7)
        hm_layout.addWidget(self.hm_save_button, 2, 6)





        self.eig_cancel_button = QtGui.QPushButton('Cancel', self)
        self.eig_cancel_button.clicked.connect(self.deleteLater)

        self.eig_save_button = QtGui.QPushButton('Save', self)
        self.eig_save_button.clicked.connect(self.eig_save)

        self.eig_fid_filename_line = QtGui.QLineEdit("Fiducial Params...")

        if self.settings.filenames[1] != "":
            self.eig_fid_filename_line.setText(self.settings.filenames[1])

        self.eig_fid_file_button = QtGui.QPushButton('...', self)
        self.eig_fid_file_button.setToolTip('Select a file')
        self.eig_fid_file_button.resize(self.eig_fid_file_button.sizeHint())
        self.eig_fid_file_button.clicked.connect(self.eig_open_fid) 

        self.eig_fish_filename_line = QtGui.QLineEdit("Fisher Matrix...")

        if self.settings.filenames[1] != "":
            self.eig_fish_filename_line.setText(self.settings.filenames[1])

        self.eig_fish_file_button = QtGui.QPushButton('...', self)
        self.eig_fish_file_button.setToolTip('Select a file')
        self.eig_fish_file_button.resize(self.eig_fish_file_button.sizeHint())
        self.eig_fish_file_button.clicked.connect(self.eig_open_fish) 


        self.eig_indices_label = QtGui.QLabel("Indices:")
        self.eig_index1 = QtGui.QLineEdit("0")
        self.eig_index2 = QtGui.QLineEdit("0")

        self.eig_names_label = QtGui.QLabel("Names:")
        self.eig_name1 = QtGui.QLineEdit()
        self.eig_name2 = QtGui.QLineEdit()

        nsig = QtCore.QString("#")
        nsig.append(QtCore.QString(1, qchar_sigma))
        self.eig_stdev_label = QtGui.QLabel(nsig)
        self.eig_stdev_count = QtGui.QLineEdit("1")

        eig_layout.addWidget(self.eig_fid_filename_line, 0, 1, 1, 6)
        eig_layout.addWidget(self.eig_fid_file_button, 0, 7)
        eig_layout.addWidget(self.eig_fish_filename_line, 1, 1, 1, 6)
        eig_layout.addWidget(self.eig_fish_file_button, 1, 7)

        eig_layout.addWidget(self.eig_indices_label, 2, 0)
        eig_layout.addWidget(self.eig_index1, 2, 2)
        eig_layout.addWidget(self.eig_index2, 2, 4)
        eig_layout.addWidget(self.eig_names_label, 3, 0)
        eig_layout.addWidget(self.eig_name1, 3, 2)
        eig_layout.addWidget(self.eig_name2, 3, 4)
        eig_layout.addWidget(self.eig_stdev_label, 4, 0)
        eig_layout.addWidget(self.eig_stdev_count, 4, 2)
        eig_layout.addWidget(self.eig_cancel_button, 5, 7)
        eig_layout.addWidget(self.eig_save_button, 5, 6)




        mcmc_cancel_button = QtGui.QPushButton('Cancel', self)
        mcmc_cancel_button.clicked.connect(self.deleteLater)

        mcmc_save_button = QtGui.QPushButton('Save', self)
        mcmc_save_button.clicked.connect(self.mcmc_save)
        self.mcmc_filename_line = QtGui.QLineEdit("MCMC Chain File...")

        if self.settings.filenames[0] != "":
            self.mcmc_filename_line.setText(self.settings.filenames[0])

        self.mcmc_file_button = QtGui.QPushButton('...', self)
        self.mcmc_file_button.setToolTip('Select a file')
        self.mcmc_file_button.resize(self.mcmc_file_button.sizeHint())
        self.mcmc_file_button.clicked.connect(self.mcmc_open_file)

        self.mcmc_1d_button = QtGui.QRadioButton('1D', self)
        self.mcmc_1d_button.toggled.connect(self.d1_enabled)

        self.mcmc_2d_button = QtGui.QRadioButton('2D', self)
        self.mcmc_2d_button.toggled.connect(self.d2_enabled)

        self.mcmc_3d_button = QtGui.QRadioButton('3D', self)
        self.mcmc_3d_button.toggled.connect(self.d3_enabled)

        mcmc_dim_group = QtGui.QButtonGroup()
        mcmc_dim_group.addButton(self.mcmc_1d_button)
        mcmc_dim_group.addButton(self.mcmc_2d_button)
        mcmc_dim_group.addButton(self.mcmc_3d_button)

        self.mcmc_indices_label = QtGui.QLabel('Indices:')
        self.mcmc_index1 = QtGui.QLineEdit('0')
        self.mcmc_index2 = QtGui.QLineEdit('0')
        self.mcmc_index3 = QtGui.QLineEdit('0')
        self.mcmc_index3.setDisabled(True)

        self.mcmc_names_label = QtGui.QLabel('Names:')
        self.mcmc_name1 = QtGui.QLineEdit()
        self.mcmc_name2 = QtGui.QLineEdit()
        self.mcmc_name3 = QtGui.QLineEdit()
        self.mcmc_name3.setDisabled(True)

        self.mcmc_draw_eig = QtGui.QCheckBox('Draw Ellipses')
        self.mcmc_draw_eig.stateChanged.connect(self.toggle_eigen_options)

        self.mcmc_eig_fid_filename_line = QtGui.QLineEdit("Eigenfidtors File...")

        if self.settings.filenames[0] != "":
            self.eig_filename_line.setText(self.settings.filenames[0])

        self.mcmc_eig_fid_file_button = QtGui.QPushButton('...', self)
        self.mcmc_eig_fid_file_button.setToolTip('Select a file')
        self.mcmc_eig_fid_file_button.resize(self.mcmc_eig_fid_file_button.sizeHint())
        self.mcmc_eig_fid_file_button.clicked.connect(self.mcmc_eig_open_fid) 

        self.mcmc_eig_fish_filename_line = QtGui.QLineEdit("Eigenfishues File...")

        if self.settings.filenames[1] != "":
            self.mcmc_eig_fish_filename_line.setText(self.settings.filenames[1])

        self.mcmc_eig_fish_file_button = QtGui.QPushButton('...', self)
        self.mcmc_eig_fish_file_button.setToolTip('Select a file')
        self.mcmc_eig_fish_file_button.resize(self.mcmc_eig_fish_file_button.sizeHint())
        self.mcmc_eig_fish_file_button.clicked.connect(self.mcmc_eig_open_fish) 

        self.mcmc_eig_fid_filename_line.setDisabled(True)
        self.mcmc_eig_fish_filename_line.setDisabled(True)
        self.mcmc_eig_fid_file_button.setDisabled(True)
        self.mcmc_eig_fish_file_button.setDisabled(True)

        self.mcmc_draw_fiducial = QtGui.QCheckBox('Draw Fiducial Point') 
        self.mcmc_draw_max = QtGui.QCheckBox('Draw Max Likelihood Point')

        self.mcmc_fid_color_button = QtGui.QPushButton()
        self.mcmc_fid_color_button.setStyleSheet("background-color:#{:02x}{:02x}{:02x}".format(((self.settings.colors[0] >> 16)&255), ((self.settings.colors[0] >> 8)&255), ((self.settings.colors[0])&255)) ) 
        self.mcmc_fid_color_button.pressed.connect(self.sel_fid_col)

        self.mcmc_max_color_button = QtGui.QPushButton()
        self.mcmc_max_color_button.setStyleSheet("background-color:#{:02x}{:02x}{:02x}".format(((self.settings.colors[1] >> 16)&255), ((self.settings.colors[1] >> 8)&255), ((self.settings.colors[1])&255) )) 
        self.mcmc_max_color_button.pressed.connect(self.sel_max_col)

        mcmc_layout.addWidget(self.mcmc_filename_line, 0, 1, 1, 6)
        mcmc_layout.addWidget(self.mcmc_file_button, 0, 7)

        mcmc_layout.addWidget(self.mcmc_1d_button, 1, 1, 1, 2)
        mcmc_layout.addWidget(self.mcmc_2d_button, 1, 3, 1, 2)
        mcmc_layout.addWidget(self.mcmc_3d_button, 1, 5, 1, 2)

        mcmc_layout.addWidget(self.mcmc_indices_label, 2, 0)
        mcmc_layout.addWidget(self.mcmc_index1, 2, 2)
        mcmc_layout.addWidget(self.mcmc_index2, 2, 4)
        mcmc_layout.addWidget(self.mcmc_index3, 2, 6)

        mcmc_layout.addWidget(self.mcmc_names_label, 3, 0)
        mcmc_layout.addWidget(self.mcmc_name1, 3, 2)
        mcmc_layout.addWidget(self.mcmc_name2, 3, 4)
        mcmc_layout.addWidget(self.mcmc_name3, 3, 6)

        mcmc_layout.addWidget(self.mcmc_draw_eig, 4, 2, 1, 3) 
        mcmc_layout.addWidget(self.mcmc_eig_fid_filename_line, 5, 1, 1, 5)
        mcmc_layout.addWidget(self.mcmc_eig_fid_file_button, 5, 6)
        mcmc_layout.addWidget(self.mcmc_eig_fish_filename_line, 6, 1, 1, 5)
        mcmc_layout.addWidget(self.mcmc_eig_fish_file_button, 6, 6)

        mcmc_layout.addWidget(self.mcmc_draw_fiducial, 7, 2, 1, 3)
        mcmc_layout.addWidget(self.mcmc_fid_color_button, 7, 6)

        mcmc_layout.addWidget(self.mcmc_draw_max, 8, 2, 1, 3)
        mcmc_layout.addWidget(self.mcmc_max_color_button, 8, 6)

        mcmc_layout.addWidget(mcmc_cancel_button, 9, 7)
        mcmc_layout.addWidget(mcmc_save_button, 9, 6)




        cs_tab.setLayout(cs_layout)
        hm_tab.setLayout(hm_layout)
        eig_tab.setLayout(eig_layout)
        mcmc_tab.setLayout(mcmc_layout)

        tabs.addTab(cs_tab, "CosmoSurvey")
        tabs.addTab(hm_tab, "Heatmap")
        tabs.addTab(eig_tab, "Eigen Ellipses")
        tabs.addTab(mcmc_tab, "MCMC Chain")

        self.layout = QtGui.QVBoxLayout()

        self.layout.addWidget(tabs)

        self.setLayout(self.layout)

        self.open()
    
    def cs_save(self):
        self.settings.types[0] = 0
        self.settings.filenames[0] = self.cs_filename_line.text()
        self.settings.log = self.cs_log_button.isChecked()
        self.close()

    def hm_save(self):
        self.settings.types[0] = 1
        self.settings.filenames[0] = self.hm_filename_line.text()
        self.settings.log = self.hm_log_button.isChecked()
        self.close()

    def eig_save(self):
        self.settings.types[0] = 2
        self.settings.filenames[0] = self.eig_fid_filename_line.text()
        self.settings.filenames[1] = self.eig_fish_filename_line.text()
        self.close()

    def mcmc_save(self):
        self.settings.types[0] = 3
        self.settings.filenames[0] = self.mcmc_filename_line.text()
        self.settings.filenames[1] = self.mcmc_eig_fid_filename_line.text()
        self.settings.filenames[2] = self.mcmc_eig_fish_filename_line.text()
        if self.mcmc_2d_button.pressed():
            self.settings.types[1] = 0
        else:
            self.settings.types[1] = 1
        self.settings.indices[0] = int(self.mcmc_index1.text())
        self.settings.indices[1] = int(self.mcmc_index2.text())
        if self.settings.types[1] == 1:
            self.settings.indices[2] = int(self.mcmc_index3.text())

        self.settings.names[0] = self.mcmc_names1.text()
        self.settings.names[1] = self.mcmc_names2.text()
        if self.settings.types[1] == 1:
            self.settings.names[2] = self.mcmc_names3.text()

        self.settings.draw_eig = self.mcmc_draw_eig.isChecked()
        self.settings.draw_fid = self.mcmc_draw_fiducial.isChecked()
        self.settings.draw_max = self.mcmc_draw_max.isChecked()
        self.close()

    def cs_open_file(self):
        self.cs_filename_line.setText(QtGui.QFileDialog.getOpenFileName(self, 'Open File', '/home'))
    def hm_open_file(self):
        self.hm_filename_line.setText(QtGui.QFileDialog.getOpenFileName(self, 'Open File', '/home'))
    def eig_open_fid(self):
        self.eig_fid_filename_line.setText(QtGui.QFileDialog.getOpenFileName(self, 'Open File', '/home'))
    def eig_open_fish(self):
        self.eig_fish_filename_line.setText(QtGui.QFileDialog.getOpenFileName(self, 'Open File', '/home'))
    def mcmc_open_file(self):
        self.mcmc_filename_line.setText(QtGui.QFileDialog.getOpenFileName(self, 'Open File', '/home'))
    def mcmc_eig_open_fid(self):
        self.mcmc_eig_fid_filename_line.setText(QtGui.QFileDialog.getOpenFileName(self, 'Open File', '/home'))
    def mcmc_eig_open_fish(self):
        self.mcmc_eig_fish_filename_line.setText(QtGui.QFileDialog.getOpenFileName(self, 'Open File', '/home'))

    def disable_3d_options(self):
        self.mcmc_index3.setDisabled(True)
        self.mcmc_name3.setDisabled(True)

    def enable_3d_options(self):
        self.mcmc_index3.setEnabled(True)
        self.mcmc_name3.setEnabled(True)

    def enable_2d_options(self):
        self.mcmc_index2.setEnabled(True)
        self.mcmc_name2.setEnabled(True)

    def disable_2d_options(self):
        self.mcmc_index2.setDisabled(True)
        self.mcmc_name2.setDisabled(True)

    def d1_enabled(self, e):
        if e:
            self.disable_2d_options()
            self.disable_3d_options()

    def d2_enabled(self, e):
        if e:
            self.enable_2d_options()
            self.disable_3d_options()

    def d3_enabled(self, e):
        if e:
            self.enable_2d_options()
            self.enable_3d_options()

    def toggle_eigen_options(self, e):
        if e == 2:
            self.mcmc_eig_fid_filename_line.setEnabled(True)
            self.mcmc_eig_fid_file_button.setEnabled(True)
            self.mcmc_eig_fish_filename_line.setEnabled(True)
            self.mcmc_eig_fish_file_button.setEnabled(True)
        else:
            self.mcmc_eig_fid_filename_line.setDisabled(True)
            self.mcmc_eig_fid_file_button.setDisabled(True)
            self.mcmc_eig_fish_filename_line.setDisabled(True)
            self.mcmc_eig_fish_file_button.setDisabled(True)

    def cs_selected(self, e):
        self.settings.types[0] = 0
        self.setup_ui()
    def hm_selected(self, e): 
        self.settings.types[0] = 1
        self.setup_ui()
    def eig_selected(self, e):
        self.settings.types[0] = 2
        self.setup_ui()
    def mcmc_selected(self, e):
        self.settings.types[0] = 3
        self.setup_ui()

    def sel_fid_col(self):
        self.settings.colors[0] = QtGui.QColorDialog.getColor().rgb() 
        self.mcmc_fid_color_button.setStyleSheet("background-color:#{:02x}{:02x}{:02x}".format(((self.settings.colors[0] >> 16)&255), ((self.settings.colors[0] >> 8)&255), ((self.settings.colors[0])&255)) ) 
        
    def sel_max_col(self):
        self.settings.colors[1] = QtGui.QColorDialog.getColor().rgb()
        self.mcmc_max_color_button.setStyleSheet("background-color:#{:02x}{:02x}{:02x}".format(((self.settings.colors[1] >> 16)&255), ((self.settings.colors[1] >> 8)&255), ((self.settings.colors[1])&255)) ) 

    @staticmethod
    def get_settings(parent=None):
        dialog = SettingsDialog(parent)
        result = dialog.exec_()
        settings = dialog.settings
        return settings


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    main = App()

    sys.exit(app.exec_())
