# Standard things

sp		:= $(sp).x
dirstack_$(sp)	:= $(d)
d		:= $(dir)

# Local Variables

OBJDIR		:= $(d)/.obj

TGTS_$(d)	:= $(d)/cosmosurvey $(d)/confgen

OBJS_$(d)	:= $(addprefix $(OBJDIR)/, types.o utils.o quadrature.o \
		   constants.o cosmology.o structure.o \
		   fisher.o likelihood.o \
		   survey.o \
		   fortlike.o )

TGT_LIB		:= $(d)/libcosmo.a
TGT_BIN		:= $(TGT_BIN) $(TGTS_$(d))
CLEAN		:= $(CLEAN) $(OBJS_$(d)) \
		   $(TGTS_$(d)) \
		   $(d)/libcosmo.a

# Local rules

$(OBJS_$(d)):		CF_TGT := -I$(d)
$(OBJS_$(d)):		| $(OBJDIR)


$(d)/libcosmo.a:	$(OBJS_$(d)) | $(DIR_LIB)
			$(ARCH)
			$(INDEX)

$(TGTS_$(d)):		$(d)/Rules.mk

$(TGTS_$(d)):		CF_TGT := -I$(d)
$(TGTS_$(d)):		LL_TGT := $(d)/libcosmo.a
$(TGTS_$(d)):		| $(DIR_VAR) $(DIR_CFG)

$(d)/cosmosurvey:	$(d)/cosmosurvey.c $(d)/libcosmo.a
		  	$(COMPLINK)
$(d)/confgen:		$(d)/confgen.c $(d)/libcosmo.a	
			$(COMPLINK)


# Standard things


d		:= $(dirstack_$(sp))
sp		:= $(basename $(sp))
		   
