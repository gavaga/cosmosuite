#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <argp.h>
#include <math.h>

#include "debug.h"
#include "constants.h"
#include "utils.h"
#include "types.h"
#include "cosmology.h"

const char * argp_program_version = "CosmoSuite Config Generator 1.1";

const char * argp_program_bug_address = "<gavin.mcdowell.14@gmail.com";

static char doc[] = 
	"This utility is used to generate a .ini file for use with a patched version of CosmoMC (see the README). It will also \
		automatically generate the .fid, .fsh, .evl, .evc files, both out of necessity, and to save cycles and \
		prevent unpredictable behaviour during the operation of CosmoMC.";

struct args
{
	char *name, *binname, *parname;
	bool place, save;
	conf_int s;
	bool log, rotate;
	double dO;
	size_t *zsubdvs, *lsubdvs;
	double *zbounds, *lbounds;
	double *h0,  *w, *wa, *om, 
		   *ol, *ob, *on, *s8,
		   *ns,  *g, *l0, *sl, *bl;
};

struct params_out
{
	conf_int s;
	double *h0,  *w, *wa, *om,
		   *ol, *ob, *on, *s8,
		   *ns,  *g, *l0, *sl, *bl;
};

static void
save_params(char * name,
			struct params_out * pout)
{
	char filename[255];
	sprintf(filename, "%s/%s.cfg", DIR_CFG, name);

	FILE * file = fopen(filename, "w");
	check(file, "Couldn't open %s. for writing.", filename);

	size_t written = fwrite(pout, sizeof(struct params_out), 1, file);
	check(written == 1, "Couldn't write to %s.", filename);

	double ** doubles = &pout->h0;
	for (size_t i = 0; i < TOTAL_PARAMS; ++i)
	{
		if (doubles[i])
		{
			written = fwrite(doubles[i], sizeof(double), 5, file);
			check(written == 5, "Couldn't write to %s.", filename);
		}
	}

	fclose(file);

	return;

error:
	return;

}

static struct params_out *
load_params(char * name)
{
	double ** doubles = NULL;
	struct params_out * pout = NULL;
	char filename[255];
	sprintf(filename, "%s/%s.cfg", DIR_CFG, name);

	FILE* file = fopen(filename, "r");
	check(file, "Couldn't open %s for writing.", filename);

	pout = (struct params_out *)calloc(1, sizeof(struct params_out));
	check_mem(pout);
	size_t read = fread(pout, sizeof(struct params_out), 1, file);
	check(read == 1, "Couldn't read from %s.", filename);

	doubles = &pout->h0;
	for (size_t i = 0; i < TOTAL_PARAMS; ++i)
	{
		if (doubles[i])
		{
			doubles[i] = (double *)calloc(5, sizeof(double));
			check_mem(doubles[i]);
			read = fread(doubles[i], sizeof(double), 5, file);
			check(read == 5, "Couldn't read from %s.", filename);
		}
	}

	fclose(file);

	return pout;

error:
	if (file)
		fclose(file);
	if (pout)
	{
		for (size_t i = 0; i < TOTAL_PARAMS; ++i)
		{
			if (doubles[i])
				free(doubles[i]);
		}
		free(pout);
	}
	return NULL;
}

static struct argp_option options[] = {
	{0, 0, 0, 0, "Utility Options:", 0},
	{"name",	'n', "NAME",		 0, "Root name to use. Outputted files are [COSMOMC_ROOT]/NAME.ini, [COSMOMC_ROOT]/paramnames/NAME.paramnames, config/NAME.cfg, and CosmoMC chains will use the root NAME.", 0},
	{"place",	400, 0,			 0, "Use this parameter if you want the .ini file and the .paramnames file to be automatically placed in the CosmoMC root.", 0},
	{"no-save",	 'N', 0,			0, "Stop automatic saving of parameter config and binnings.", 0},
	{"binnings",	'b', 0,			0, "Reference name of binnings to load. Will override binnings loaded by \"-c,--config-name\" and itself be overwritten by any explicity binning parameters.", 0},
	{"params",	  'p', 0,				 0, "Reference name of parameter set to load. Will be overridden per-parameter by command-line specified parameter distributions.", 0},
	{0, 0, 0, 0, "Survey Parameters:", 1},
	{"nz",		500, "INTS", 0,	"Redshift bin subdivisions.", 1},
	{"redshift",	502, "VALS", 0,	"Redshift bin boundaries.", 1},
	{"nl",		501, "INTS", 0,	"Richness bin subdivisions.", 1},
	{"richness",	503, "VALS", 0,	"Richness bin boundaries.", 1},
	{"dO",		'O', "DOUBLE",		0, "Survey solid angle dOmega in steradians", 1},
	{0, 0, 0, 0, "Cosmological Parameters:", 2},
	{"h0",		600, "DOUBLE",		0, "Hubble constant parameter H_0", 2},
	{"w",		601, "DOUBLE",		 0, "Dark Energy equation-of-state parameter", 2},
	{"wa",		602, "DOUBLE",		 0, "w evolution parameter", 2},
	{"ol",		 603, "DOUBLE",		 0, "Dark Energy density parameter Omega_L", 2},
	{"om",		604, "DOUBLE",		 0, "Matter density parameter Omega_M", 2},
	{"ob",		605, "DOUBLE",		 0, "Baryon density parameter Omega_B", 2},
	{"on",		 606, "DOUBLE",		 0, "Neutrino density parameter Omega_B", 2},
	{"s8",		 607, "DOUBLE",		0, "Power spectrum normalization sigma_8", 2},
	{"ns",		608, "DOUBLE",		 0, "Primordial power-law index n_s", 2},
	{"g",		 609, "DOUBLE",		 0, "Linder growth index gamma", 2},
	{"l0",		610, "DOUBLE",		 0, "Mass-richness normalization parameter lambda_0", 2},
	{"sl",		 611, "DOUBLE",		 0, "Mass-richness scatter parameter sigma_lambda", 2},
	{"bl",		 612, "DOUBLE",		 0, "Mass-richness evolution parameter beta_lambda", 2},
	{0, 0, 0, 0, "Sampling Parameters:", 3},
	{"force",	'f', 0,			0, "Recalculate Fisher Information & Eigen Decomposition, overwriting previous.", 3},
	{"log",		700, 0,			0, "Force CosmoMC to sample in logspace", 3},
	{"rotate",	'r', 0,		 0, "Force CosmoMC to sample in eigenspace", 3},
	{0}
};


static error_t
parse_opt(int key, char *arg, struct argp_state * state)
{
	struct args * args = state->input;
	char * endptr;

	static size_t bounds_alloced[] = {0, 0};
	static size_t subdvs_alloced[] = {0, 0};
	static size_t params_alloced[] = {0,0,0,0,0,0,0,0,0,0,0,0,0};

	switch (key)
	{
		case 'n': args->name = arg; break;
		case 'b': args->binname = arg; break;
				case 'p': args->parname = arg; break;
		case 'N': args->save = false; break;
		case 400: 
				  {
#ifdef COSMOMC_ROOT
					  args->place = true; break;
#else
					  fprintf(stderr, "No COSMOMC_ROOT found. Ignoring option \"-p, --place\"."); 
					  break;
#endif
				  }
		case 502:
		case 503:
			  {
				  int idx = key - 502;
				  double ** bounds = &args->zbounds;

				  /* Allocate memory for boundaries if necessary. */
				  if (bounds_alloced[idx] == 0)
				  {
					  bounds[idx] = (double *)calloc(sizeof(double), 6);
					  check_mem(bounds[idx]);
					  bounds_alloced[idx] = 6;
				  }
				  else if (bounds[idx][0] <= bounds_alloced[idx])
				  {
					  bounds[idx] = (double *)realloc(bounds[idx], sizeof(double) * (bounds_alloced[idx] + 6));
					  check_mem(bounds[idx]);
					  bounds_alloced[idx] += 6;
				  }


				  bounds[idx][0] += 1;
				  bounds[idx][(int)bounds[idx][0]] = strtod(arg, &endptr);

				  while(strchr(endptr, ','))
				  {
					  /* Allocate more memory if need be. */
					  if (bounds[idx][0] >= bounds_alloced[idx])
					  {
						  bounds[idx] = (double *)realloc(bounds[idx], sizeof(double) * (bounds_alloced[idx] + 6));
						  check_mem(bounds[idx]);	
						  bounds_alloced[idx] += 6;
					  }

					  bounds[idx][0] += 1;
					  bounds[idx][(int)bounds[idx][0]] = strtod(endptr + 1, &endptr);
				  }

				  break;
			  }

		case 500:
		case 501:
			  {
				  int idx = key - 500;
				  size_t ** subdvs = &args->zsubdvs;

				  if (subdvs_alloced[idx] == 0)
				  {
					  subdvs[idx] = (size_t *)calloc(sizeof(size_t), 6);
					  check_mem(subdvs[idx]);
					  subdvs_alloced[idx] = 6;
				  }
				  else if (subdvs[idx][0] <= subdvs_alloced[idx])
				  {
					  subdvs[idx] = (size_t *)realloc(subdvs[idx], sizeof(size_t) * (subdvs_alloced[idx] + 6));
					  check_mem(subdvs[idx]);
					  subdvs_alloced[idx] += 6;
				  }

				  subdvs[idx][0] += 1;
				  subdvs[idx][subdvs[idx][0]] = (size_t)strtod(arg, &endptr);

				  while(strchr(endptr, ','))
				  {
					  if (subdvs[idx][0] >= subdvs_alloced[idx])
					  {
						  subdvs[idx] = (size_t *)realloc(subdvs[idx], sizeof(size_t) * (subdvs_alloced[idx] + 6));
						  check_mem(subdvs[idx]);
						  subdvs_alloced[idx] += 6;
					  }

					  subdvs[idx][0] += 1;
					  subdvs[idx][subdvs[idx][0]] = (size_t)strtod(endptr + 1, &endptr);
				  }

				  break;
			  }
		case 'O': args->dO = strtod(arg, NULL); break;

		case 700: args->log = true; break;
		case 'r': args->rotate = true; break;
		case 'f': args->s |= FLAG_FORCE; break;

		case 600:
		case 601:
		case 602:
		case 603:
		case 604:
		case 605:
		case 606:
		case 607:
		case 608:
		case 609:
		case 610:
		case 611:
		case 612:
			  {
				  double ** params = &args->h0;
				  int index = key - 600;

				  if (params_alloced[index] == 0)
				  {
					  params[index] = (double *)calloc(sizeof(double), 6);
					  check_mem(params[index]);
					  params_alloced[index] = 6;
				  }

				  check(params[index][0] < params_alloced[index] - 1, "Too many arguments for %s, ignoring.", PNAMES[index]);

				  params[index][0] += 1;
				  params[index][(int)params[index][0]] = strtod(arg, &endptr);
				  
				  while(strchr(endptr, ','))
				  {
					  check(params[index][0] < params_alloced[index] - 1, "Too many arguments for %s, ignoring.", PNAMES[index]);

					  params[index][0] += 1;
					  params[index][(int)params[index][0]] = strtod(endptr+1, &endptr);
				  }
				  break;
			  }


		default: return ARGP_ERR_UNKNOWN;
	}
	return 0;

error:
	return 0;
}

static char args_doc[] = "";

static struct argp argp	 = {options, parse_opt, args_doc, doc, 0, 0, 0};

static double zbounds_defaults[] = {0.1, 1.5};
static size_t zsubdvs_defaults[] = {10};
static double lbounds_defaults[] = {10, 350};
static size_t lsubdvs_defaults[] = {10};

int
main(int ac,
	 char ** av)
{
	CVector * evl = NULL, *ptemp = NULL;
	Binning ** binnings = NULL, **tempbins = NULL;
	struct params_out * pout = NULL, *pout_other = NULL;

	size_t idx = 0;

	double ** pars = NULL;
	size_t * subdvs = NULL;


	struct args args;

	/* Set defaults */
	args.name = NULL;
	args.binname = NULL;
	args.parname = NULL;
	args.place = false;
	args.save = true;
	args.s = 0;

	CParams * cparams = lcdm();
	SParams * sparams = wfirst();

	args.dO = sparams->dOmega;

	/* Make sure all values are NULL */
	pars = &args.h0;
	for (size_t i = 0; i < TOTAL_PARAMS; ++i)
		pars[i] = NULL;


	/* Actually parse arguments */
	argp_parse(&argp, ac, av, 0, 0, &args);

	/* Build binnings */
	if (args.binname)
	{
		binnings = load_binnings(args.binname, 2);
	}
	else
	{
		binnings = (Binning **)calloc(sizeof(Binning *), 2);
		binnings[0] = Binning_make(1, zbounds_defaults, zsubdvs_defaults);
		binnings[1] = Binning_make(1, lbounds_defaults, lsubdvs_defaults);
	}

	if (args.zbounds)
	{
		Binning_destroy(binnings[0]);
		if (args.zsubdvs)
			binnings[0] = Binning_make(args.zsubdvs[0], &args.zbounds[1], &args.zsubdvs[1]);
		else
		{
			subdvs = (size_t *)malloc(sizeof(size_t) * ((size_t)args.zbounds[0] - 1));
			check_mem(subdvs);

			for (int i = 0; i < (int)args.zbounds[0] - 1; ++i)
			{
				subdvs[i] = 1;
			}

			binnings[0] = Binning_make((size_t)args.zbounds[0] - 1, &args.zbounds[1], subdvs);
			free(subdvs);
		}
	}

	if (args.lbounds)
	{
		Binning_destroy(binnings[1]);
		if (args.lsubdvs)
			binnings[1] = Binning_make(args.lsubdvs[0], &args.lbounds[1], &args.lsubdvs[1]);
		else
		{
			subdvs = (size_t *)malloc(sizeof(size_t) * ((size_t)args.lbounds[0] - 1));
			check_mem(subdvs);

			for (size_t i = 0; i < (size_t)args.lbounds[0] - 1; ++i)
				subdvs[i] = 1;

			binnings[1] = Binning_make((size_t)args.lbounds[0] - 1, &args.lbounds[1], subdvs);
			free(subdvs);
		}
	}
	/* */


	/* Build params */
	if (args.parname)
	{
		pout_other = load_params(args.parname);
		check(pout_other, "Parameters of %s not loaded properly.", args.parname);

		double **newparams = &pout_other->h0;
		if (pout_other->s & FLAG_ROTATED)
		{
			ptemp = CVector_make(count_params(pout_other->s));
			tempbins = load_binnings(args.parname, 2);

			conf_int ts;
			for (size_t k = 0; k < 3; ++k)
			{
				idx = 0;
				for (size_t i = 0; i < TOTAL_PARAMS; ++i)
				{
					if (newparams[i])
					{
						CVector_set(ptemp, idx, newparams[i][k]);
						++idx;
					}
				}

				ts = pout_other->s;
				normalize_params(args.parname, ptemp, &ts, tempbins);

				idx = 0;
				for (size_t i = 0; i < TOTAL_PARAMS; ++i)
				{
					if (newparams[i])
					{
						newparams[i][k] = CVector_get(ptemp, idx);
						++idx;
					}
				}
			}


			for (size_t b = 0; b < 2; ++b)
				Binning_destroy(tempbins[b]);

			CVector_destroy(ptemp);
			ptemp = NULL;
		}


		pars = &pout->h0;
		for (size_t i = 0; i < TOTAL_PARAMS; ++i)
		{
			if (newparams[i])
			{
				if (pars[i])
					free(pars[i]);
				pars[i] = newparams[i];
			}
		}
		free(pout_other);
	}

	/* Parse parameter binput */
	pars = &args.h0;
	double **parsout = &pout->h0;
	for (size_t i = 0; i < TOTAL_PARAMS; ++i)
	{
		if (pars[i])
		{
			if (parsout[i])
				free(parsout[i]);

			parsout[i] = (double *)calloc(5, sizeof(double));

			size_t nargs = (size_t)pars[0];
			
			parsout[i][0] = pars[i][1];
			switch(nargs)
			{
				case 3:
					{
						parsout[i][1] = pars[i][1] - pars[i][2];
						parsout[i][2] = pars[i][1] + pars[i][2];
						parsout[i][3] = pars[i][2];
						parsout[i][4] = pars[i][2];

						break;
					}
				case 4:
					{
						parsout[i][1] = pars[i][2];
						parsout[i][2] = pars[i][3];
						parsout[i][3] = pars[i][4];
						parsout[i][4] = pars[i][4];

						break;
					}
				case 5:
					{
						parsout[i][1] = pars[i][2];
						parsout[i][2] = pars[i][3];
						parsout[i][3] = pars[i][4];
						break;
					}
				default:
					sentinel("Bad number of parameters for %s.", PNAMES[i]);
			}


		}	
	}

	ptemp = CVector_make(count_params(pout->s));
	pars = &pout->h0;
	for (size_t j = 0; j < 3; ++j)
	{
		idx = 0;
		for (size_t ix = 0; ix < TOTAL_PARAMS; ++ix)
		{
			CVector_set(ptemp, idx, pars[ix][j]);
			++idx;
		}

		setup_params(args.name, ptemp, &pout->s, binnings, args.log, args.rotate);

		idx = 0;
		for (size_t ix = 0; ix < TOTAL_PARAMS; ++ix)
		{
			pars[ix][j] = CVector_get(ptemp, idx);
			++idx;
		}
	}
	CVector_destroy(ptemp);

	/* Set step sizes based on eigenvalues if rotating. */
	if (args.rotate)
	{
		evl = CVector_make(count_params(pout->s));
		load_eigen(args.name, pout->s, binnings, NULL, evl);

		idx = 0;
		for (size_t ix = 0; ix < TOTAL_PARAMS; ++ix)
		{
			if (pars[ix])
			{
				check(CVector_get(evl, idx) > 0, "Bad eigenvalue.");
				pars[ix][3] = sqrt(1.0 / CVector_get(evl, idx));
				pars[ix][4] = pars[ix][3];
			}
		}

		CVector_destroy(evl);
	}

	if (args.save)
	{
		save_params(args.name, pout);
		save_binnings(args.name, 2, binnings);
	}

	pars = &pout_other->h0;

/* Print args struct if debugging */	
#ifdef DEBUG
	printf("struct args\n \
	Name: %s\n \
	Binning Name:  %s\n \
	Parameter Name: %s\n \
	Place: %d, Save: %d\n \
	Configuration: 0x%X\n \
	dOmega: %2.f\n",
		args.name, args.binname, args.parname,
		args.place, args.save,
		pout->s, 
		args.dO);

	for (size_t k = 0; k < TOTAL_PARAMS; ++k)
	{
		printf("\t%2s: [%7.2f, %7.2f, %7.2f, %7.2f, %7.2f]\n", PNAMES[k], pars[k][0], pars[k][1], pars[k][2], pars[k][3], pars[k][4]);
	}

	print_binnings(2, binnings);

#endif

	//TODO: Clean up
	return 0;

/* If anything goes wrong, clean up everything that still exists and return nonzero */
error:
	if (cparams)
		CParams_destroy(cparams);
	if (sparams)
		SParams_destroy(sparams);

	pars = &args.h0;
	for (int i = 0; i < TOTAL_PARAMS; ++i)
	{
		if (pars[i])
			free(pars[i]);
	}

	return 1;
}
