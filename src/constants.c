#include "constants.h"

const double RHO_CRIT   = 2.77466274901e11; 	/**< Critical Density of the universe in M_sun h^2 Mpc^-3 */
const double H100       = 3.24077928534e-18; 	/**< \f$h \cdot s^{-1}\f$ */
const double C          = 9.71561187787e-15;	/**< Speed of light in \f$Mpc \cdot s^{-1}\f$ */
const double CM_PER_MPC = 3.0856775854e24;	/**< cm/Mpc conversion factor */
const double PI         = 3.14159265358979;	/**< \f$\pi\f$ */
const double M_SCALE    = 1e15;			/**< Scale Factor in \f$h^{-1} \cdot M_{sun} */
const double M_8        = 0.595070983403;	/**< \f$10^{15} \,\Omega_M\cdot h^{-1}\cdot M_{sun}\f$ */
const double THETA_CMB   = 1.0094444444;	/**< \f$ T_{CMB}/2.7K\f$ using \f$T_{CMB}=2.7255 K\f$ */
const double Z_EQ       = 24077.44063;		/**< Redshift of Matter-Radiation equality,
						  *  \f$ z_{eq} = 2.5\times 10^4 \cdot \theta_{CMB}^{-4} \f$
						  *  in \f$ \Omega_M \cdot h^2 \f$
						  */

const double Z_P        = 0.47; 		/**< Fiducial redshift used by WFIRST for ... */
const double N_NU       = 3.0;			/**< Number of massive neutrinos */

/** \brief Parameter Names
 *
 * List of parameter names which can currently be freed. 
 */
const char * const PNAMES[] = {"h0", "w", "wa", "ol", "om", "ob", "on", "s8", "ns", "g", "l0", "sl", "bl"};


const uint64_t m1	= 0x5555555555555555;
const uint64_t m2	= 0x3333333333333333;
const uint64_t m4	= 0x0f0f0f0f0f0f0f0f;
const uint64_t m8	= 0x00ff00ff00ff00ff;
const uint64_t m16	= 0x0000ffff0000ffff;
const uint64_t m32	= 0x00000000ffffffff;
const uint64_t hff	= 0xffffffffffffffff;
const uint64_t h01	= 0x0101010101010101;
