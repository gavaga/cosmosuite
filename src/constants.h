/** \file constants.h
 *  \brief Cosmological Constants
 * 
 * This file contains definitions of cosmological constants which are used throughout the project.
 */

#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <stdint.h>


#define SETTINGS_OFFSET 0
#define NSETTINGS	4

#define PARAM_OFFSET 	4
#define TOTAL_PARAMS	13

#define PARAMS(X)	((X) >> PARAM_OFFSET)&0xFFFFFFFF	/**< Get the PARAMS section of the settings conf_int */
#define SETTINGS(X)	((X) >> SETTINGS_OFFSET)&0xF		/**< Get the SETTINGS section of the conf_int */

#define FLAG_LOG 	(1L << SETTINGS_OFFSET)
#define FLAG_ROTATED	(1L << (SETTINGS_OFFSET + 1))
#define FLAG_FORCE 	(1L << (SETTINGS_OFFSET + 2))
#define FLAG_NORMALIZED (1L << (SETTINGS_OFFSET + 3))

/** 
 * Generates a bitmask for a given parameter index.
 */
#define PFLAG(X)	(1L << ((X) + PARAM_OFFSET))

#define FLAG_H0 	(1L << PARAM_OFFSET)
#define FLAG_W  	(1L << (PARAM_OFFSET + 1))
#define FLAG_WA 	(1L << (PARAM_OFFSET + 2))
#define FLAG_OL 	(1L << (PARAM_OFFSET + 3))
#define FLAG_OM 	(1L << (PARAM_OFFSET + 4))
#define FLAG_OB 	(1L << (PARAM_OFFSET + 5))
#define FLAG_ON 	(1L << (PARAM_OFFSET + 6))
#define FLAG_S8 	(1L << (PARAM_OFFSET + 7))
#define FLAG_NS 	(1L << (PARAM_OFFSET + 8))
#define FLAG_G  	(1L << (PARAM_OFFSET + 9))
#define FLAG_L0 	(1L << (PARAM_OFFSET + 10))
#define FLAG_SL 	(1L << (PARAM_OFFSET + 11))
#define FLAG_BL 	(1L << (PARAM_OFFSET + 12))


/* cosmological constants */
extern const double RHO_CRIT;	
extern const double H100;
extern const double C;
extern const double CM_PER_MPC;
extern const double PI;	
extern const double M_SCALE;
extern const double M_8;
extern const double THETA_CMB;
extern const double Z_EQ;
extern const double Z_P;
extern const double N_NU;

/* parameter names */
extern const char * const PNAMES[];


extern const uint64_t m1;
extern const uint64_t m2;
extern const uint64_t m4;
extern const uint64_t m8;
extern const uint64_t m16;
extern const uint64_t m32;
extern const uint64_t hff;
extern const uint64_t h01;

#endif /* CONSTANTS_H */
