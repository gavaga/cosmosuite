/** \file cosmology.c
 * \brief \f$\Lambda\text{CDM}\f$ Methods
 *
 * A set of functions relating to the underlying cosmological
 * model, including models for cluster mass-observable relations.
 */

#include "cosmology.h"

#include <math.h>
#include <stdio.h>

#include "constants.h"
#include "types.h"
#include "quadrature.h"
#include "debug.h"

extern CParams * theta_g;


/** \brief \f$\Lambda\text{CDM}\f$
 * 
 * Function to return fiducial set of cosmological parameters.
 * (Note: Any freed parameters will be overwritten in an optimization setting.)
 * These are the standard \f$\Lambda\text{CDM}\f$ parameters, in addition to 
 * a fiducial set of model parameters for the mass-observable relation.
 */
CParams *
lcdm()
{
	CParams * theta = CParams_make();

	theta->h0	  = 0.6704;	
	theta->w           = -1.022;
	theta->wa          = -0.18;
	theta->omega_l_0   = 0.6817;
	theta->omega_M_0   = 0.3183;
	theta->omega_b_0   = 0.046;
	theta->omega_nu_0  = 0.0013;
	theta->sigma_8     = 0.8347;
	theta->ns          = 0.9619;
	theta->gamma       = 0.55;
	theta->lambda_0    = 60.0;
	theta->sig_lambda  = 0.25;
	theta->beta_lambda = 0.0;

	return theta;
} // lcdm()


/** \brief WFIRST Parameters
 *
 * Function to generate the fiducial set of survey parameters, with defaults
 * set to the most recent set of mission parameters for WFIRST.
 * (Note: these parameters are likely to be overwritten, but not freed, in an
 * experimentation setting.)
 */
SParams*
wfirst()
{
	SParams * surv = SParams_make();
	SParams_defaults(surv);
	return surv;
} // wfirst()


/** \brief E
 *
 * Function to calculate evolution parameter \f$E(z)=\frac{H(z)}{H_0}\f$
 */
double
E(const double z 		/**< [in] Redshift. */)
{
	check(z >= 0, "Redshift of %.2f not in expected range.", z);

	double a = 1. / (1. + z);
	double a_p = 1. / (1. + 0.47);
	double wpower = 3. * (1 + theta_g->w + theta_g->wa*a_p);
	double wexp = - 3. * theta_g->wa * (a_p - a);
	double omega_kappa_0 = 1.0 - theta_g->omega_M_0 - theta_g->omega_l_0;
	return sqrt( theta_g->omega_M_0 / pow(a, 3.) + theta_g->omega_l_0 / pow(a, wpower) * exp(wexp) + omega_kappa_0 / pow(a, 2.));
	
error:
	return 0.0;
} // E()


/** \brief \f$\Omega_M(z)\f$
 *
 * Function to calculate the matter density parameter \f$\Omega_M(z)\f$
 * as a function of redshift.
 */
double
omega_M(const double z 		/**< [in] Redshift */)
{
	check(z >= 0, "Redshift of %.2f not in expected range.", z);

	double Ez = E(z);
	double om = theta_g->omega_M_0 * pow(1. + z, 3.) / pow(Ez, 2.);	
//	cout << "omega_M z: " << z << " E: " << Ez << " om: " << om << endl;
	return om;

error:
	return 0.0;
} // omega_M()


/** \brief \f$\Omega_\Lambda(z)\f$
 *
 * Calculates the Dark Energy Density parameter \f$\Omega_\Lambda(z)\f$
 * as a function of redshift.
 */
double
omega_lambda(const double z 	/**< [in] Redshift. */)
{
	check(z >= 0, "Redshift of %.2f not in expected range.", z);

	double Ez = E(z);
	return theta_g->omega_l_0 / pow(Ez, 2.);

error:
	return 0.0;
} // omega_lambda()


/** \brief \f$\frac{\partial r}{\partial z}\f$
 * Derivative with respect to redshift of the comoving distance r.
 * \units \f$c\cdot H_0^{-1}\f$
 */
double
dr_dz(double z, 		/**< [in] Redshift. */
      void* argv		/**< [in] Unused argument to allow integration by GSL algorithms. */)
{
	(void) argv;
	
	check(z >= 0, "Redshift of %.2f not in expected range.", z);

	double Ez = E(z);
	return 1. / Ez;

error:
	return 0.0;
} // dr_dz()


/** \brief Coordinate (comoving) distance
 * 
 * \f[
 *     \int_{z_1}^{z_2}\frac{\partial r}{\partial z}\,dz
 * \f]
 * \units \f$c\cdot H_0^{-1}\f$
 * \sa codist
 */
double
codist_range(const double z1,	/**< [in] Redshift lower bound. */
	     const double z2 	/**< [in] Redshift upper bound. */)
{
	check(z1 >= 0 && z2 >= 0, "Redshift range [%.2f,%.2f] invalid.", z1, z2);
	check(z2 > z1, "z2 is greater than z1.");

	return gsl_qag(&dr_dz, z1, z2);

error:
	return 0.0;
} // codist(1)


/** \brief Coordinate (comoving) distance
 * 
 * \f[
 * 	r_{co}(z) = \int_{0}^{z}\frac{\partial r}{\partial z}\,dz
 * \f]
 * \units \f$c\cdot H_0^{-1}\f$
 * \sa codist_range dr_dz
 */
double
codist(const double z 		/**< [in] Redshift. */)
{
	check(z >= 0, "Redshift of %.2f not in expected range.", z);

	return codist_range(0., z);

error:
	return 0.0;
} // codist(2)


/** \brief \f$D_\kappa(z)\f$
 *
 * Curvature-corrected comoving distance \f$D_\kappa\f$
 * Three-term Taylor-series expansion approximation used
 * is accurate to \f$10^{-4}\f$ for \f$\frac{D_\kappa}{R_\kappa} < 1\f$
 * \units \f$c\cdot H_0^{-1}\f$
 */
double
d_kappa(const double z 		/**< [in] Redshift. */)
{
	check(z >= 0, "Redshift of %.2f not in expected range.", z);

	double d_kappa = codist(z);
	double omega_kappa_0 = 1. - theta_g->omega_M_0 - theta_g->omega_l_0;
	if (fabs(omega_kappa_0) > 0.0001)
	{
		double r_kappa = 1. / sqrt(fabs(omega_kappa_0));
		double sgn = omega_kappa_0 / fabs(omega_kappa_0);
		double x = d_kappa / r_kappa;
		return d_kappa * (1. - (sgn * pow(x, 2.) / 6.) + (pow(x, 4.) / 120.) );
	}
	return d_kappa;

error:
	return 0.0;
} // d_kappa()


/** \brief \f$D_L(z)\f$
 *
 * Luminosity distance of redshift \f$z\f$.
 * \units \f$c\cdot H_0^{-1}\f$
 */
double
lumdist(const double z		/**< [in] Redshift. */)
{
	check(z >= 0, "Redshift of %.2f not in expected range.", z);

	return (1. + z) * d_kappa(z);

error:
	return 0.0;
} // lumdist()


/** \brief \f$ D_A(z) \f$
 *
 * Angular size distance of redshift z.
 * \units \f$c\cdot H_0^{-1}\f$ 
 */
double
sizdist(const double z 		/**< [in] Redshift. */)
{
	check(z >= 0, "Redshift of %.2f not in expected range.", z);
	
	return d_kappa(z) / (1. + z);

error:
	return 0.0;
} // sizdist()


/* Integrand interface to dV_dOmega_dz to allow GSL to integrate it */
double
dV_dOmega_dz_int(double z,
	         void* argv)
{
	check(z >= 0, "Redshift of %.2f not in expected range.", z);

	(void) argv;

	double rkappa = d_kappa(z);
	double Ez = E(z);
	return pow(rkappa, 2) / Ez;

error:
	return 0.0;
} // dV_dOmega_dz_int()


/** \brief \f$\frac{\partial^2 V}{\partial z\,\partial\Omega}\f$
 *
 * where V is the comoving volume element.
 * \units \f$(c\cdot H_0^{-1})^3\f$
 */
double
dV_dOmega_dz(const double z	/**< [in] Redshift. */)
{
	check(z >= 0, "Redshift of %.2f not in expected range.", z);

	double rkappa = d_kappa(z);
	double Ez = E(z);
	return pow(rkappa, 2) / Ez;

error:
	return 0.0;
} // dV_dOmega_dz(1)


/** \brief Comoving volume element \f$dV\f$
 *
 * Comoving volume element between \f$z_1\f$ and \f$z_2\f$ within 
 * solid angle \f$d\Omega\f$.
 * \f[
 * 	dV = \int_{z_1}^{z_2}\frac{\partial^2 V}{\partial z\,\partial\Omega}\,dz\f$
 * \f]
 * \units \f$ (c\cdot H_0^{-1})^3 \f$
 * \sa dV_dOmega_dz
 */
double
dV(const double z1,		/**< [in] Redshift lower bound. */
   const double z2,		/**< [in] Redshift upper bound. */
   const double dOmega		/**< [in] Survey solid angle. */)
{
	check(z1 >= 0 && z2 >= 0, "Redshift range [%.2f,%.2f] invalid.", z1, z2);
	check(z2 > z1, "Range mismatch: z1 > z2.");
	check(dOmega > 0, "Solid angle must be positive-definite.");

	return gsl_qag(dV_dOmega_dz_int, z1, z2) * dOmega;

error:
	return 0.0;
} // dV()


/** \brief \f$p_i\f$
 *
 * Factor by which neutrinos alter the logarithmic growth rate of
 * perturbations.
 * \f[
 * 	p_i : \begin{cases} \text{CDM} &i=1 \\
 * 			    \text{CDM + Baryons} & i\neq 1 \end{cases}
 * \f]
 */
double
p_i(const int i 		/**< [in] Index */)
{
	//TODO: Add index checks.
	check(i == 0 || i != 0, "Invalid index provided.");
	double f;
	if (i == 1)
	{
		f = (theta_g->omega_M_0 - theta_g->omega_nu_0) / theta_g->omega_M_0;
	}
	else
	{
		f = (theta_g->omega_M_0 - theta_g->omega_nu_0 - theta_g->omega_b_0) / theta_g->omega_M_0;
	}
	return 0.25 * (5 - sqrt(1. + (24. * f)));

error:
	return 0.0;
} // p_i()


/** \brief \f$y_d\f$
 */
double
y_d()
{
	return (1. + Z_EQ) / (1. + z_d());
} // y_d()


/** \brief \f$z_d\f$
 *
 * Redshift of baryon release from radiation
 * Compton drag.
 */
double
z_d()
{
	return 1291. * ( pow(theta_g->omega_M_0 * pow(theta_g->h0, 2.), 0.251) / (1. + 0.659 * pow(theta_g->omega_M_0 * pow(theta_g->h0, 2.), 0.828))) * (1. + b1() * pow(theta_g->omega_b_0 * pow(theta_g->h0, 2.), b2()));
} // z_d()


/** \brief \f$b_1\f$
 *
 * First constant for calculation of \f$z_d\f$
 * \sa z_d
 */
double
b1()
{
	return 0.313 * pow(theta_g->omega_M_0 * pow(theta_g->h0, 2.), -0.419) * (1. + 0.607 * pow(theta_g->omega_M_0 * pow(theta_g->h0, 2.), 0.674));
} // b1()


/** \brief \f$b_2\f$
 *
 * Second constant for calculation of \f$z_d\f$
 */
double
b2()
{
	return 0.238 * pow(theta_g->omega_M_0 * pow(theta_g->h0, 2.), 0.223);
} // b2()


/** \brief \f$s\f$
 */
double
s()
{
	return 44.5 * log(9.83 / (theta_g->omega_M_0 * pow(theta_g->h0, 2.))) / sqrt(1. + 10. * pow(theta_g->omega_M_0 * pow(theta_g->h0, 2.), 0.75));
} // s()


/** \brief \f$q(k)\f$
 * Convert scale factor \f$k\f$ to wavenumber parameter \f$q\f$.
 */
double
q_of_k(const double k 		/**< [in] Scale factor. */)
{
	//TODO: Add check for k
	check(k >= 0, "Scale factor must be positive semidefinite.");
	return k * THETA_CMB * THETA_CMB * pow(theta_g->omega_M_0 * theta_g->h0 * theta_g->h0, -1);

error:
	return 0.0;
}


/** \brief \f$k(q)\f$
 * Convert wavenumber parameter \f$q\f$ to scale factor \f$k\f$.
 */
double
k_of_q(const double q 		/**< [in] Wavenumber. */)
{
	//TODO: Add check for q
	check(q >= 0, "Wavenumber must be positive semidefinite.");
	return q * (theta_g->omega_M_0 * theta_g->h0 * theta_g->h0) * pow(THETA_CMB, -2.);

error:
	return 0.0;
}
