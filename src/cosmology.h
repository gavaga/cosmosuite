#ifndef COSMOLOGY_H // include guard
#define COSMOLOGY_H

#include "types.h"

CParams *
lcdm();

CMatrix *
lcdm_priors();

SParams *
wfirst();

double
E(double z);

double
omega_M(double z);
		 
double
omega_lambda(double z);


double
dr_dz(double z,
      void* argv);

double
codist_range(double z1,
	     double z2);

double
codist(double z);	

double
d_kappa(double z);

double
lumdist(double z);

double
sizdist(double z);

double
dV_dOmega_dz_int(double z,
		 void* argv);

double
dV_dOmega_dz(double z);

double
dV(double z1,
   double z2,
   double domega);

double
p_i(int i);

double
y_d();

double
z_d();

double
b1();

double
b2();

double
s();

double
q_of_k(double k);

double
k_of_q(double q);

#endif /* COSMOLOGY_H */
