#include <stdlib.h>
#include <stdio.h>
#include <argp.h>
#include <stdbool.h>

#include "debug.h"
#include "types.h"
#include "utils.h"
#include "cosmology.h"
#include "survey.h"


const char * argp_program_version = 
	"CosmoSurvey 3.0";
const char * argp_program_bug_address = 
	"<gavin.mcdOwell.14@gmail.com>";

static char doc[] = 
	"Simulate galaxy cluster surveys in optical wavelengths.\vFor more information please view the README in the root of the CosmoSurvey source directory.";

struct arguments
{
	char *name, *binning_name;
	double dO;
       	double h0,  w, wa, om, 
	       ol, ob, on, s8,
	       ns,  g, l0, sl, bl;
	bool flat;
	double * zbounds, * lbounds;
	size_t * zsubdvs, * lsubdvs;
};

static struct argp_option options[] = {
	{0, 0, 0, 0, "Program Options:", 5},
	{"save",	's', "NAME", 0, "Save binnings to be associated with NAME.", 5},
	{"binnings",	'b', "NAME", 0, "Binnings name. If it exists, binnings will be loaded from it and overridden if other binnings are provided.", 5},
	{0, 0, 0, 0, "Survey Parameters:", 10}, 
	{"nz",		500, "INTS", 0,	"Redshift bin subdivisions.", 10},
	{"redshift",	502, "VALS", 0,	"Redshift bin boundaries.", 10},
	{"nl",		501, "INTS", 0,	"Richness bin subdivisions.", 10},
	{"richness",	503, "VALS", 0,	"Richness bin boundaries.", 10},
	{"dO",		510, "VAL", 0,	"Survey solid angle dOmega in steradians", 10},
	{0, 0, 0, 0, "Cosmological Parameters:", 20}, 
	{"h0",		601, "VAL", 0,	"Hubble constant parameter H_0", 20},
	{"w",		602, "VAL", 0,	"Dark Energy equation-of-state parameter", 20},
	{"wa",		603, "VAL", 0,	"w evolution parameter", 20},
	{"om",		604, "VAL", 0,	"Matter density parameter Omega_M", 20},
	{"ol",		605, "VAL", 0,	"Dark Energy density parameter Omega_L", 20},
	{"flat",	606, 0, 0,	"Sets Omega_L = 1 - Omega_M if true", 20},
	{"ob",		607, "VAL", 0, 	"Baryon density parameter Omega_B", 20},
	{"on", 		608, "VAL", 0,	"Neutrino density parameter Omega_Nu", 20},
	{"s8", 		609, "VAL", 0,	"Power spectrum normalization sigma_8", 20},
	{"ns", 		610, "VAL", 0,	"Primordial power-law index n_s", 20},
	{"g",		611, "VAL", 0, 	"Linder growth index gamma", 20},
	{"l0",		612, "VAL", 0, 	"Mass-richness normalization parameter lambda_0", 20},
	{"sl", 		613, "VAL", 0,	"Mass-richness scattar parameter sigma_lambda", 20},
	{"bl",		614, "VAL", 0,	"Mass-richness evolution parameter beta_lambda", 20},
	{0}
};

static error_t
parse_opt (int key, char *arg, struct argp_state * state)
{
	struct arguments * arguments = state->input;
	char * endptr;

	static size_t bounds_alloced[2] = {0, 0};
	static size_t subdvs_alloced[2] = {0, 0};

	switch (key)
	{
		case 's': arguments->name = arg; break;
		case 'b': arguments->binning_name = arg; break;
		case 502:
		case 503:
			  {
				  int idx = key - 502;
				  double ** bounds = &arguments->zbounds;

                  /* Allocate memory for boundaries if necessary. */
                  if (bounds_alloced[idx] == 0)
                  {
                      bounds[idx] = (double *)calloc(sizeof(double), 6);
                      check_mem(bounds[idx]);
                      bounds_alloced[idx] = 6;
                  }
                  else if (bounds[idx][0] <= bounds_alloced[idx])
                  {
                      bounds[idx] = (double *)realloc(bounds[idx], sizeof(double) * (bounds_alloced[idx] + 6));
                      check_mem(bounds[idx]);
                      bounds_alloced[idx] += 6;
                  }


				  bounds[idx][0] += 1;
				  bounds[idx][(int)bounds[idx][0]] = strtod(arg, &endptr);

				  while(strchr(endptr, ','))
				  {
					  /* Allocate more memory if need be. */
					  if (bounds[idx][0] >= bounds_alloced[idx])
					  {
						  bounds[idx] = (double *)realloc(bounds[idx], sizeof(double) * (bounds_alloced[idx] + 6));
						  check_mem(bounds[idx]);	
						  bounds_alloced[idx] += 6;
					  }

					  bounds[idx][0] += 1;
					  bounds[idx][(int)bounds[idx][0]] = strtod(endptr + 1, &endptr);
				  }

				  break;
			  }

		case 500:
		case 501:
			  {
				  int idx = key - 500;
				  size_t ** subdvs = &arguments->zsubdvs;

				  if (subdvs_alloced[idx] == 0)
				  {
					  subdvs[idx] = (size_t *)calloc(sizeof(size_t), 6);
					  check_mem(subdvs[idx]);
					  subdvs_alloced[idx] = 6;
				  }
				  else if (subdvs[idx][0] <= subdvs_alloced[idx])
				  {
					  subdvs[idx] = (size_t *)realloc(subdvs[idx], sizeof(size_t) * (subdvs_alloced[idx] + 6));
					  check_mem(subdvs[idx]);
					  subdvs_alloced[idx] += 6;
				  }

				  subdvs[idx][0] += 1;
				  subdvs[idx][subdvs[idx][0]] = (size_t)strtod(arg, &endptr);

				  while(strchr(endptr, ','))
				  {
					  if (subdvs[idx][0] >= subdvs_alloced[idx])
					  {
						  subdvs[idx] = (size_t *)realloc(subdvs[idx], sizeof(size_t) * (subdvs_alloced[idx] + 6));
						  check_mem(subdvs[idx]);
						  subdvs_alloced[idx] += 6;
					  }

					  subdvs[idx][0] += 1;
					  subdvs[idx][subdvs[idx][0]] = (size_t)strtod(endptr + 1, &endptr);
				  }

				  break;
			  }

		case 510: arguments->dO = strtod(arg, NULL); break;
		case 601: arguments->h0 = strtod(arg, NULL); break;
		case 602: arguments->w = strtod(arg, NULL); break;
		case 603: arguments->wa = strtod(arg, NULL); break;
		case 604: arguments->om = strtod(arg, NULL); break;
		case 605: arguments->ol = strtod(arg, NULL); break;
		case 606: arguments->ol = 1 - arguments->om; break;
		case 607: arguments->ob = strtod(arg, NULL); break;
		case 608: arguments->on = strtod(arg, NULL); break;
		case 609: arguments->s8 = strtod(arg, NULL); break;
		case 610: arguments->ns = strtod(arg, NULL); break;
		case 611: arguments->g = strtod(arg, NULL); break;
		case 612: arguments->l0 = strtod(arg, NULL); break;
		case 613: arguments->sl = strtod(arg, NULL); break;
		case 614: arguments->bl = strtod(arg, NULL); break;
		default: return ARGP_ERR_UNKNOWN;			  
	}
	return 0;
	
error:
	return 1;
}

static char args_doc[] = "";

static struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};

static double zbounds_defaults[] = {0.1, 1.5};
static size_t zsubdvs_defaults[] = {10};
static double lbounds_defaults[] = {10, 350};
static size_t lsubdvs_defaults[] = {10};

int
main (int ac,
      char ** av)
{
        CMatrix * N = NULL;
	struct arguments arguments;
	CParams * cparams = lcdm();
	SParams * sparams = wfirst();

	arguments.name = NULL;
	arguments.binning_name = NULL;

	arguments.dO = sparams->dOmega;

	arguments.h0 = cparams->h0;
	arguments.w = cparams->w;
	arguments.wa = cparams->wa;
	arguments.ol = cparams->omega_l_0;
	arguments.om = cparams->omega_M_0;
	arguments.ob = cparams->omega_b_0;
	arguments.on = cparams->omega_nu_0;
	arguments.s8 = cparams->sigma_8;
	arguments.ns = cparams->ns;
	arguments.g = cparams->gamma;
	arguments.l0 = cparams->lambda_0;
	arguments.sl = cparams->sig_lambda;
	arguments.bl = cparams->beta_lambda;

	arguments.zbounds = NULL;
	arguments.zsubdvs = NULL;
	arguments.lbounds = NULL;
	arguments.lsubdvs = NULL;

	argp_parse(&argp, ac, av, 0, 0, &arguments);

	if (arguments.zsubdvs)
		check(arguments.zbounds, "Must provide bounds for subdivisons.");
	if (arguments.lsubdvs)
		check(arguments.lbounds, "Must provide bounds for subdivisions.");
	if (arguments.zbounds && arguments.zsubdvs)
		check(arguments.zsubdvs[0] + 1 == (size_t)arguments.zbounds[0], "Number of redshift bounds must be exactly one more than number of redshift subdivisions.");
	if (arguments.lbounds && arguments.lsubdvs)
		check(arguments.lsubdvs[0] + 1 == (size_t)arguments.lbounds[0], "Number of richness bounds must be exactly one more than number of richness subdivisions.");

	Binning ** binnings;

	if (arguments.binning_name)
	{
		binnings = load_binnings(arguments.binning_name, 2);
	}
	else
	{
		binnings = (Binning **)calloc(sizeof(Binning *), 2);
		binnings[0] = Binning_make(1, zbounds_defaults, zsubdvs_defaults);
		binnings[1] = Binning_make(1, lbounds_defaults, lsubdvs_defaults);
	}

	if (arguments.zbounds)
	{
		Binning_destroy(binnings[0]);
		if (arguments.zsubdvs)
			binnings[0] = Binning_make(arguments.zsubdvs[0], &arguments.zbounds[1], &arguments.zsubdvs[1]);
		else
		{
			size_t * subdvs = (size_t *)malloc(sizeof(size_t) * ((size_t)arguments.zbounds[0] - 1));
			check_mem(subdvs);

			for (int i = 0; i < (int)arguments.zbounds[0] - 1; ++i)
			{
				subdvs[i] = 1;
			}

			binnings[0] = Binning_make((size_t)arguments.zbounds[0] - 1, &arguments.zbounds[1], subdvs);
			free(subdvs);
		}
	}

	if (arguments.lbounds)
	{
		Binning_destroy(binnings[1]);
		if (arguments.lsubdvs)
			binnings[1] = Binning_make((size_t)arguments.lsubdvs[0], &arguments.lbounds[1], &arguments.lsubdvs[1]);
		else
		{
			size_t * subdvs = (size_t *)malloc(sizeof(size_t) * ((size_t)arguments.lbounds[0] - 1));
			check_mem(subdvs);

			for (size_t i = 0; i < (size_t)arguments.lbounds[0] - 1; ++i)
				subdvs[i] = 1;

			binnings[1] = Binning_make((size_t)arguments.lbounds[0] - 1, &arguments.lbounds[1], subdvs);
			free(subdvs);
		}
	}

#ifdef DEBUG
	print_binnings(2, binnings);
#endif /* DEBUG */

	if (arguments.name)
		save_binnings(arguments.name, 2, binnings);

	sparams->dOmega = arguments.dO;

	cparams->h0 = arguments.h0;
	cparams->w = arguments.w;
	cparams->wa = arguments.wa;
	cparams->omega_l_0 = arguments.ol;
	cparams->omega_M_0 = arguments.om;
	cparams->omega_b_0 = arguments.ob;
	cparams->omega_nu_0 = arguments.on;
	cparams->sigma_8 = arguments.s8;
	cparams->ns = arguments.ns;
	cparams->gamma = arguments.g;
	cparams->lambda_0 = arguments.l0;
	cparams->sig_lambda = arguments.sl;
	cparams->beta_lambda = arguments.bl;

	N = gen_N(cparams, sparams, binnings);

	CMatrix_print(N);

	CMatrix_destroy(N);
	if (arguments.zbounds)
		free(arguments.zbounds);
	if (arguments.zsubdvs)
		free(arguments.zsubdvs);
	if (arguments.lbounds)
		free(arguments.lbounds);
	if (arguments.lsubdvs)
		free(arguments.lsubdvs);

	return 0;

error:
	if (N)
		CMatrix_destroy(N);
	if (arguments.zbounds)
		free(arguments.zbounds);
	if (arguments.zsubdvs)
		free(arguments.zsubdvs);
	if (arguments.lbounds)
		free(arguments.lbounds);
	if (arguments.lsubdvs)
		free(arguments.lsubdvs);
	return 1;
}
