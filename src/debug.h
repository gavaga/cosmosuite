#ifndef DEBUG_H
#define DEBUG_H


#include <stdio.h>
#include <errno.h>
#include <string.h>

#ifdef DEBUG
/** \brief Debugging Print Statement
 *
 * Macro which will only resolve to an actual print statement if 
 * -DDEBUG is passed to make.
 */
#define debug(...) 						        	\
	do {									\
		fprintf(stderr, "DEBUG %s:%d:%s(): ", __FILE__, 	        \
			__LINE__, __func__);                                    \
                fprintf(stderr, __VA_ARGS__);			                \
                fprintf(stderr, "\n");                                          \
	} while(0)
#else
#define debug(...)
#endif

#define clean_errno() (errno == 0 ? "None" : strerror(errno))

#define log_err(...)                                                            \
        do {                                                                    \
                fprintf(stderr, "[ERROR] (%s:%d:%s(): errno: %s) ", 		\
				__FILE__, __LINE__, __func__, clean_errno());   \
                fprintf(stderr, __VA_ARGS__);                                   \
                fprintf(stderr, "\n");                                          \
        } while(0)

#define log_warn(...)                                                           \
        do {                                                                    \
                fprintf(stderr, "[WARN] (%s:%d:%s(): errno: %s) ", 		\
				 __FILE__, __LINE__, __func__, clean_errno());  \
                fprintf(stderr, __VA_ARGS__);                                   \
                fprintf(stderr, "\n");                                          \
        } while(0);

#define log_info(...)                                                           \
        do {                                                                    \
                fprintf(stderr, "[INFO] (%s:%d:%s()) ",                         \
			   	 __FILE__, __LINE__, __func__);                 \
                fprintf(stderr, __VA_ARGS__);                                   \
                fprintf(stderr, "\n");                                          \
        } while(0)

#define check(A, ...) if (!(A)) { log_err(__VA_ARGS__); errno=0; goto error; }

#define sentinel(...) { log_err(__VA_ARGS__); errno=0; goto error; }

#define check_mem(A) check((A), "Out of memory.")

#define check_debug(A, ...) if(!(A)) { debug(__VA_ARGS__); errno=0; goto error; }

#endif /* DEBUG_H */
