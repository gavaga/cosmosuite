#include "fisher.h"

#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "debug.h"
#include "constants.h"
#include "types.h"
#include "utils.h"
#include "cosmology.h"
#include "likelihood.h"
#include "survey.h"

CMatrix *
fisher_chain_rule(char * name,			/**< [in] Experiment Reference name. */
		  conf_int s,			/**< [in] Configuration. */
		  Binning ** binnings		/**< [in] Dynamic binnings. */)
{
	debug("Computing Fisher Information");

	size_t nparams = count_params(s);
	size_t nz = calc_total_bins(binnings[0]);
	size_t nl = calc_total_bins(binnings[1]);

	CParams * theta;
	SParams * default_surv = wfirst();

	/* Ensure default params aren't renormalized. */
	s &= !(FLAG_FORCE | FLAG_LOG | FLAG_ROTATED);
	s |= FLAG_NORMALIZED;

	double log_step = -3.5;
	CVector * fparams = load_fiducial_params(s);
	CVector * p;

	size_t sizes[] = {nz, nl, nparams};
	CTensor * dlnx_dlntheta = CTensor_make(3, sizes);

	CMatrix * fbins = load_fiducial_bins_named(name, binnings);
	for (size_t i = 0; i < nparams; ++i)
	{
		p = CVector_copy(fparams);
		CVector_set(p,i, CVector_get(fparams,i) * (1 - pow(10, log_step)));
		theta = generate_cparams(name, p->data, s, binnings);
		CMatrix * low = gen_N(theta, default_surv, binnings);
		CParams_destroy(theta);
		CVector_destroy(p);

		p = CVector_copy(fparams);
		CVector_set(p, i, CVector_get(fparams,i) * (1 + pow(10, log_step)));
		theta = generate_cparams(name, p->data, s, binnings);
		CMatrix * high = gen_N(theta, default_surv, binnings);
		CParams_destroy(theta);
		CVector_destroy(p);

		for (size_t j = 0; j < nz; ++j)
		{
			for (size_t k = 0; k < nl; ++k)
			{
				if (s & FLAG_LOG)
				{
					CTensor_set(dlnx_dlntheta, (CMatrix_get(high, j, k) - CMatrix_get(low, j, k)) / (2 * CMatrix_get(fbins, j, k) * pow(10., log_step)), j, k, i);
				}
				else
				{
					CTensor_set(dlnx_dlntheta, (CMatrix_get(high, j, k) - CMatrix_get(low, j, k)) / (2 * pow(10., log_step)), j, k, i);
				}
			}
		}

		CMatrix_destroy(low);
		CMatrix_destroy(high);
	}
	CVector_destroy(fparams);


	CMatrix * fisher = CMatrix_make(nparams, nparams);

	for (size_t i = 0; i < nparams; ++i)
	{
		for (size_t j = i; j < nparams; ++j)
		{
			CMatrix_set(fisher, i, j, 0.0);

			for (size_t k = 0; k < nz; ++k)
			{
				for (size_t l = 0; l < nl; ++l)
				{
					double inc = CTensor_get(dlnx_dlntheta, k, l, i) * CTensor_get(dlnx_dlntheta, k, l, j);
					CMatrix_set(fisher, i, j, CMatrix_get(fisher, i, j) + (2.0 * CMatrix_get(fbins, k, l))*inc);
				}
			}
			CMatrix_set(fisher, j, i, CMatrix_get(fisher, i, j));
		}
	}

	CTensor_destroy(dlnx_dlntheta);
	CMatrix_destroy(fbins);

	return fisher;

//error:
//	return NULL;
}
