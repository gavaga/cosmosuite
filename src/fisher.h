#ifndef FISHER_H // include guard
#define FISHER_H

#include "types.h"

/**
 * A set of algorithms for calculating the Fisher Information of
 * the likelihood function.
 *
 * @author GMM
 * @date 2014-07-18
 */


/**
 * Fastest Fisher Information calculation utlizing the
 * chain rule factors of the log-gaussian.
 *  
 * @changelog 2014-07-29 GMM generalized to allow optional use of log-space, as
 * well as to accomodate arbitrary parameter set.
 */
CMatrix *
fisher_chain_rule(char * name,
		  conf_int s,
		  Binning ** binnings);

/**
 * Slower than the chain rule algorithm, but incorporates iterative
 * deepening in order to increase accuracy. Expected most accurate. Incomplete.
 *
 * @TODO Convert from Fortran and finish debugging.
 */
CMatrix *
fisher_chain_rule_recursive(conf_int s,
			    Binning ** binnings);

#endif // include guard
