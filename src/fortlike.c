#include <stdlib.h>
#include <stdint.h>

#include "debug.h"
#include "constants.h"
#include "types.h"
#include "utils.h"
#include "likelihood.h"

CParams * theta_g;
SParams * survey_g;


float
likelihood_(char * name,
	    double * free_params_raw,
	    uint64_t * s,
	    double * survey_params,
	    char * binnings_fname)
{
	if (!name)
		log_warn("No name provided, recomputation every cycle is slow.");
	if (!free_params_raw)
		log_info("No parameter list provided: what are we doing here?");

	if (!s)
	{
 		fprintf(stderr, "No configuration provided.");
		exit(1);
	}
	if (!survey_params)
	{
		fprintf(stderr, "No survey parameters provided.");
		exit(1);
	}
	if (!binnings_fname)
	{
		fprintf(stderr, "No binnings filename provided.");
		exit(1);
	}


	Binning ** binnings = load_binnings(binnings_fname, 2);
	if (!binnings)
	{
		fprintf(stderr, "Something went wrong loading the binnings from %s, exiting.", binnings_fname);
		exit(1);
	}

	conf_int key = (conf_int)*s;
	CParams * theta = generate_cparams(name, free_params_raw, key, binnings);
	if (!theta)
	{
		fprintf(stderr, "Something went wrong evalutating the raw parameter list, exiting.");
		exit(1);
	}

	SParams * surv = generate_sparams(survey_params);
	if (!surv)
	{
		fprintf(stderr, "Something went wrong evalutating the survey parameters, exiting.");
		exit(1);
	}

	float loglike = likelihood(name, theta, surv, binnings);

	CParams_destroy(theta);
	SParams_destroy(surv);
	for (int i = 0; i < 2; ++i)
	{
		Binning_destroy(binnings[i]);
	}
	free(binnings);
	
	return loglike;
}
