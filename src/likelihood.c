/** \file likelihood.c
 * A set of functions for calculating the joint
 * inverse log-Gaussian likelihood representing
 * the "distance" of a data set from the 
 * fiducial point in \f$\underline{\theta}\f$ space.
 *
 * \author GMM
 * \date 2014-0*-**
 */

#include "likelihood.h"

#include "constants.h"
#include "types.h"
#include "cosmology.h"
#include "survey.h"
#include "utils.h"
#include "debug.h"

#include <stdio.h>
#include <math.h>


/**
 * Function which calculates the normalization of the Gaussian
 * distribution.
 */
double
log_gaussian_norm(const double n		/**< [in] Standard deviation or fiducial bin value. */)
{
	check(n > 0, "Fiducial bin value must be positive-definite.");
	
	return log(1. / sqrt(8. * atan(1.) * n));

error:
	return 0.0;
}


/** \brief \f$\log\mathcal{N}\f$
 *
 * Function which calculates the log of a one-dimensional normal distribution with mean \f$x\f$
 * and variance \f$n^2\f$.
 */
double
log_gaussian(const double x,			/**< [in] Mean or test bin value. */
	     const double n			/**< [in] \f$\sigma\f$ or fiducial bin value. */)
{
	check(x > 0 && n > 0, "Bin values must be positive definite.");

	double norm = log_gaussian_norm(n);
	return -(norm - 0.5 * (pow(x - n, 2) / n));

error:
	return 0.0;
}


/**
 * Function, primarily called through FORTRAN
 * by function Generic_GetLogLikeMain from calclike.f90 in 
 * project CosmoMC, which calculates, given the following 
 * set of cosmological and model parameters, the likelihood 
 * that this this parameter set would have generated the fiducial observables
 * by calculating the joint probability over the set of 
 * \f$n_z \times n_\lambda\f$ bins.
 *
 * \f[
 *	\mathcal{L} = \sum_{i=1}^{n_z\cdot n_\lambda} \log \mathcal{N}(x_i | N_i, N_i^2)
 * \f]
 *
 * \changelog 2014-07-29 name changed to likelihood and format 
 * changed to internal standard, as <code>fortlike.cpp</code> has the interface
 * to CosmoMC.
 * \changelog 2014-07-18 <code>likelihood_</code> and <code>likelihood_var_</code> combined into
 * single function; handling of eigenvectors for rotation changed for 
 * more streamlining.
 */
float
likelihood(char * name,				/**< [in] Name of experiment's defaults to load. If NULL, will recompute fiducial bins. */
	   CParams * theta,			/**< [in] Cosmological parameters to pass to CosmoSurvey. */
	   SParams * survey,			/**< [in] Survey parameters to pass to CosmoSurvey. */
	   Binning ** binnings			/**< [in] Set of binnings for z, l, etc. */)
{
	CMatrix * bins = NULL;
	CMatrix * fid = NULL;

	check(theta, "No cosmological parameters provided.");
	check(survey, "No survey parameters provided.");
	check(binnings, "No binnings provided.");

	bins = gen_N(theta, survey, binnings);
	if (name)
	{
		fid  = load_fiducial_bins_named(name, binnings);
	}
	else
	{
		fid = compute_fiducial_bins_dynamic(binnings);
	}

	size_t nz = calc_total_bins(binnings[0]);
	size_t nl = calc_total_bins(binnings[1]);

	float likelihood = 0.0;

	for (size_t i = 0; i < nz; ++i)
	{
		for (size_t j = 0; j < nl; ++j)
		{
			likelihood += (float)log_gaussian(CMatrix_get(bins, i, j), CMatrix_get(fid, i, j));
		}
	}

	CMatrix_destroy(bins);
	CMatrix_destroy(fid);

	return likelihood;

error:
	if (bins)
		CMatrix_destroy(bins);
	if (fid)
		CMatrix_destroy(fid);
	return 0.0;
}
