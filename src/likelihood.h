#ifndef LIKELIHOOD_H // include guard
#define LIKELIHOOD_H

#include "types.h"

double
calc_gaussian_norm(const double n);

double
log_gaussian(const double x, 
	     const double n);

float
likelihood(char * name,
	   CParams * theta,
	   SParams * survey,
	   Binning ** binnings);

#endif // include guard
