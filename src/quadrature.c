#include "quadrature.h"

#include <stdio.h>

#include "gsl/gsl_integration.h"
#include "cuba.h"

#include "debug.h"

/** \brief Relative error parameter
 *
 * Integrations are required to be accurate to 0.1% */
const double EPSREL = 1e-3;


/** \brief Proper Integration
 *
 * This function is a wrapper for the gsl_integration_qag routine, which computes
 * proper numerical integrals.
 */
double
gsl_qag(double (*dy_dx)(double,
			void*),		/**< [in] Integrand. */
	const double x1,		/**< [in] Lower bound. */
	const double x2			/**< [in] Upper bound. */)
{
	check(dy_dx, "No integrand provided.");

	gsl_function f;
	f.function = dy_dx;
	f.params = NULL;
	double epsabs = 0.0;
	size_t limit = 1000;
	int key = 1;

	double result, error;

	gsl_integration_workspace* w = gsl_integration_workspace_alloc(limit);
	
	gsl_integration_qag(&f, x1, x2, epsabs, EPSREL, limit, key, w, &result, &error);

	gsl_integration_workspace_free(w);
	return result;

error:
	return 0.0;
}


/** \brief Proper Integration with additional parameters
 *
 * This function is the same as gsl_qag, only is allows additional required parameters to be passed
 * to the inegrant through the void* parameter.
 */
double
gsl_qag_extra(double (*dy_dx)(double,
			      void*),	/**< [in] Integrand. */
	      const double x1,		/**< [in] Lower bound. */
	      const double x2,		/**< [in] Upper bound. */
	      void* params		/**< [in] Vector of additional parameters. */)
{
	check(dy_dx, "No integrand provided.");

	gsl_function f;
	f.function = dy_dx;
	f.params = (void *) params;
	double epsabs = 0.0;
	size_t limit = 1000;
	int key = 1;

	double result, error;

	gsl_integration_workspace* giw = gsl_integration_workspace_alloc(limit);
	
	gsl_integration_qag(&f, x1, x2, epsabs, EPSREL, limit, key, giw, &result, &error);
	gsl_integration_workspace_free(giw);

	return result;

error:
	return 0.0;
}


/** \brief Improper Integration
 *
 * Wrapper for GSL's various improper integral routines, including
 * qagi, qagiu, and qagil which correspond respectively to bounds of
 * \f$(-\infty,\infty), \f$[x_1,\infty)\f$, and \f$(-\infty, x_1]\f$.
 */
double
gsl_qagi(double (*dy_dx)(double,
			 void*),	/**< [in] Integrand. */
	 const double x1,		/**< [in] Bound, if applicable. This parameter is ignored if \f$\text{inf}=0\f$. */
	 const int inf			/**< [in] Type of improper integration. */)
{
	check(dy_dx, "No integrand provided.");
	check(inf == 1 || inf == -1 || inf == 0, "Invalid integration type provided.");

	gsl_function f;
	f.function = dy_dx;
	f.params = NULL; 

	double result, error;

	double epsabs = 0.0;
	size_t limit = 100000;
	gsl_integration_workspace* giw = gsl_integration_workspace_alloc(limit);

	switch (inf)
	{
		case 1:
			gsl_integration_qagiu(&f, x1, epsabs, EPSREL, limit, giw, &result, &error);
			break;
		case -1:
			gsl_integration_qagil(&f, x1, epsabs, EPSREL, limit, giw, &result, &error);
			break;
		default:
			gsl_integration_qagi(&f, epsabs, EPSREL, limit, giw, &result, &error);
			break;
	}

	gsl_integration_workspace_free(giw);

	return result;

error:
	return 0.0;
}


/** \brief Improper Integration with additional parameters
 *
 * Same as gsl_qagi, but again with the additional option of passing a vector of 
 * additional parameters to the integrand through the void* parameter.
 */
double
gsl_qagi_extra(double (*dy_dx)(double,
			       void*),	/**< [in] Integrand. */
	       const double x,		/**< [in] Bound. */
	       const int inf,		/**< [in] Type of improper integration. */
               void* argv		/**< [in] Vector of additional parameters. */)
{
	check(dy_dx, "No integrand provided.");
	check(inf == 0 || inf == -1 || inf == 1, "Invalid integration type provided.");

	gsl_function f;
	f.function = dy_dx;
	f.params = argv;

	double result, error;

	double epsabs = 0.0;
	size_t limit = 100000;
	gsl_integration_workspace * giw = gsl_integration_workspace_alloc(limit);

	switch (inf)
	{
		case 1:
			gsl_integration_qagiu(&f, x, epsabs, EPSREL, limit, giw, &result, &error);
			break;
		case -1:	
			gsl_integration_qagil(&f, x, epsabs, EPSREL, limit, giw, &result, &error);
			break;
		default:	
			gsl_integration_qagi(&f, epsabs, EPSREL, limit, giw, &result, &error);
			break;
	}

	gsl_integration_workspace_free(giw);
	return result;

error:
	return 0.0;
}


/** \brief Cuhre
 *
 * This is a wrapper function for Feynarts' Cuba library's deterministic
 * multidimensional integrator Cuhre, which utilizes cubature rules
 * along with globally adaptive subdivision to obtain an accurate an deterministic
 * result efficiently.
 *
 * The integrand is integrated over an ndim-dimensional hypercube, so the integrand
 * must be scaled appropriately using the chain rule.
 */
void
cuba_cuhre(int (*integrand)(const int*,
			    const double[], 
			    const int*, 
			    double[], 
			    void*), 		/**< [in] Integrand. */
	   const int ndim, 			/**< [in] Number of dimensions. */
	   const double tol,			/**< [in] Tolerance (i.e. relative error). */ 
	   void* userdata,			/**< [in] Vector of additional parameters to pass to the integrand. */ 
	   double* val,				/**< [out] Result. */
	   double* err,				/**< [out] Est. error of val. */
	   int* n				/**< [out] Number of times integrand was called. */)
{
	int ncomp = 1;
	double integral[ncomp], error[ncomp], prob[ncomp];

	check(integrand, "No integrand provided.");
	check(ndim > 0, "ndim must be positive.");
	check(tol > 0, "Relative error must be positive definite.");
	check(val, "Val must be non-NULL");

	int nvec = 1;
	double epsrel = tol;
	double epsabs = 1e-12;
	int verbose = 0;
	int mineval = 0;
	int maxeval = 1000000;
	int key = 0;
	int nregions, neval, fail;

	Cuhre(ndim, ncomp, integrand, userdata, nvec, epsrel, epsabs,
			verbose, mineval, maxeval, key,
		       	NULL, NULL,   // statefile, spin
			&nregions, &neval, &fail, integral, error, prob);

	*val = integral[0];
	if (err)
		*err = error[0];
	if (n)
		*n = neval;

	return;

error:
	*val = 0;
	return;
}	
