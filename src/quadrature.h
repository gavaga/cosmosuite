#ifndef QUADRATURE_H // include guard
#define QUADRATURE_H

double
gsl_qag(double (*dy_dx)(double,
			void*), 
        double x1, 
	double x2);

double
gsl_qag_extra(double (*dy_dx)(double,
			      void*), 
	      double x1, 
	      double x2, 
	      void* params);

double
gsl_qagi(double (*dy_dx)(double,
			 void*),
	 double x,
	 int inf);

double
gsl_qagi_extra(double (*dy_dx)(double,
			       void*),
	       double x,
	       int inf,
	       void* argv);

void
cuba_cuhre(int (*integrand)(const int*,
			    const double[],
			    const int*,
			    double[],
			    void*),
	   int ndim,
	   double tol,
	   void* userdata,
	   double* val,
	   double* err,
	   int* n);

#endif //include guard
