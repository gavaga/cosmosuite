#include "structure.h"

#include <stdio.h>
#include <math.h>

#include "constants.h"
#include "quadrature.h"
#include "cosmology.h"
#include "debug.h"

extern CParams * theta_g;


/** \brief Integrand Linder
 *
 * used in the calculation of the growth factor \f$G(z)\f$.
 */
double
integrand_linder(double z,		/**< [in] Redshift. */
		 void* argv		/**< [in] Empty; allows integration. */)
{
	(void) argv;

	check(z >= 0, "Redshift of %.2f is not in expected range.", z);

	return (pow(omega_M(z), theta_g->gamma) - 1.)/ (1. + z);

error:
	return 0.0;
} // integrand_linder()


/** \brief Growth factor
 *
 * calculation using the Linder growth function. 
 *
 * \changelog 2011-06-14 JLM: Name changed from f_G to f_G_Linder.
 */
double
g_linder(const double z 		/**< [in] Redshift. */)
{
	check(z >= 0, "Redshift of %.2f is not in expected range.", z);

	return exp(gsl_qagi(&integrand_linder, z, 1))/(1. + z);

error:
	return 0.0;
} // g_linder()


/** \brief \f$ \ln(\sigma)^{-1}\f$
 *
 * Function to generate \f$\sigma(M,z)\f$, the RMS mass-fluctuation
 * amplitude on mass scale \f$M\f$, where \f$M\f$ is in units of \f$h^{-1}\cdot M_{sun} \f$
 * and at redshift \f$z\f$.
 */
double
ln_inv_sigma(const double M, 		/**< [in] Mass. */
	     const double z		/**< [in] Redshift. */)
{
	check(M > 0, "Mass must be positive-definite.");
	check(z >= 0, "Redshift of %.2f is not in expected range.", z);

	double lnsigz0 = ln_sigma_z0(M); // use the fitting formula
	double gzm = g_zM(z, M) / g_zM(0., M); // scale-dependent growth
	return -lnsigz0 - log(gzm);

error:
	return 0.0;
} // ln_inv_sigma()


/** \brief \f$\alpha_{eff}\f$
 * Function to generate 
 * \f[
 * 	\alpha_{eff} = \frac{d\log\left(\frac{1}{\sigma(M)}\right)}{d\log(M)}
 * \f]
 * for chain-rule factor in dn_dv_dm
 */
double
alpha_eff(const double M,		/**< [in] Mass. */
	  const double z		/**< [in] Redshift. */)
{
	check(M > 0, "Mass must be positive-definite.");
	check(z >= 0, "Redshift of %.2f is not in expected range.", z);

	double dlnsigz0_dlnM = d_lnsigz0_d_lnM(M);
	double dlngzM_dlnM = d_lngzM_d_lnM(z, M);
	double alpha = -dlnsigz0_dlnM - dlngzM_dlnM;
	return alpha;

error:
	return 0.0;
} // alpha_eff()


/** \brief \f$\log(\sigma(M,z=0))\f$
 *
 * Shape-governing function \f$ \sigma(M,z)\mid_{z=0} \f$
 * Fitting function for \f$ \sigma(M,z)\mid_{z=0} \f$ goes here.
 */ 
double
ln_sigma_z0(const double M		/**< [in] Mass. */)
{
	check(M > 0, "Mass must be positive-definite.");

	double lnMM8 = lnM_M8norm(M);
	double lns8 = log(theta_g->sigma_8);
	double lin = lnsig_z0_lincoeff();
	double quad = lnsig_z0_quadcoeff();
	double third = lnsig_z0_thirdcoeff();
	double x = lnMM8;

	return lns8 + lin*x + quad*x*x + third*x*x*x;

error:
	return 0.0;
} // ln_sigma_z0()


/** \brief \f$ \frac{d\log(\sigma(M,z))}{d\log M}\mid_{z=0} \f$
 *
 * Function to generate derivative of ln_sigma_z0 with
 * respect to \f$\log M\f$. For \f$ \frac{d\log(\sigma(M,z)^{-1})}{d\log M}\mid_{z=0}\f$
 * just multiply by -1.
 */
double
d_lnsigz0_d_lnM(const double M		/**< [in] Mass. */)
{
	check(M > 0, "Mass must be positive-definite.");

	double lin = lnsig_z0_lincoeff();
	double quad = lnsig_z0_quadcoeff();
	double third = lnsig_z0_thirdcoeff();
	double lnMM8 = lnM_M8norm(M);
	double x = lnMM8;
	return lin + 2*quad*x + 3*third*x*x;

error:
	return 0.0;
} // d_lnsigz0_d_lnM()


/** \brief \f$ \log(M/M_8) \f$
 *
 * Function to generate \f$ \log(M/M_8) \f$ given a mass in 
 * \f$ M_{sun}\cdot h^{-1} \f$
 */
double
lnM_M8norm(const double M		/**< [in] Mass. */)
{
	check(M > 0, "Mass must be positive-definite.");

	double M8 = M_8*(theta_g->omega_M_0); // units 1e15 M_sol h^-1
	M8 = M8 * M_SCALE; // units h^-1
	return log(M/M8); // dimensionless

error:
	return 0.0;
} // lnM_M8norm()


/** \brief \f$ \log(\sigma(M,z))\mid_{z=0} \f$ coefficient
 * which is linear in \f$ \log(M/M_8) \f$.
 */
double
lnsig_z0_lincoeff()
{
	double dn = theta_g->ns - 1.0;
	double ob = theta_g->omega_b_0;
	double om = theta_g->omega_M_0;
	double on = theta_g->omega_nu_0;
	double a0 = -1.24e-1;
	double a1 = -1.30e-1;
	double a2 =  4.48e-1;
	double a3 = -6.43e-1;
	double a4 =  1.46;
	double a5 =  1.74e+1;
	double a6 =  6.41e-1;
	double a7 = -3.83e-4;
	return a0+a1*dn+a2*ob+a3*om+a4*on+a5*on*on+a6*om*om+a7*on*om;
} // lnsig_z0_lincoeff()


/** \brief \f$ \log(\sigma(M,z))\mid_{z=0} \f$ coefficient
 * which is quadratic in \f$ \log(M/M_8) \f$
 */
double
lnsig_z0_quadcoeff()
{
	double ob  = theta_g->omega_b_0;
	double om  = theta_g->omega_M_0;
	double on  = theta_g->omega_nu_0;
	double a8  = -7.76e-3;
	double a9  =  4.12e-2;
	double a10 = -1.65e-2;
	double a11 =  1.50e-1;
	double a12 = -2.94e-1;
	return a8+a9*ob+a10*om+a11*on+a12*on*om;
} // lnsig_z0_quadcoeff()


/** \brief \f$ \log(\sigma(M,z))\mid_{z=0} \f$ coefficient
 * 
 * which is to the third order in \f$ \log(M/M_8) \f$.
 */
double
lnsig_z0_thirdcoeff()
{
	return -3.83e-4;
} // lnsig_z0_thirdcoeff()


/** \brief \f$ g_{zM} \f$
 *
 * Function which describes scale dependent growth,
 * modeled after expression in Eisenstein and Hu (1997) eq 13 D_cbnu(z,q).
 */
double
g_zM(const double z, 		/**< [in] Redshift. */
     const double M		/**< [in] Mass. */)
{
	check(z >= 0, "Redshift of %.2f is not in expected range.", z);
	check(M > 0, "Mass must be positive-definite.");

	if (theta_g->omega_nu_0 == 0.0)
	{
		return g_linder(z); // scale-independent growth
	}
	else
	{
		double fcb = 1. - theta_g->omega_nu_0/theta_g->omega_M_0;
		double yfs = y_fs(M); // free-streaming scale for neutrinos
		double gz  = g_linder(z); // scale-independent growth
		double pcb = p_cb(); // describes how growth is suppressed by neutrinos
		double gzM = pow(pow(fcb,(0.7/pcb)) + pow(gz/(1.+yfs), 0.7),pcb/0.7);
		return gzM * pow(gz, 1. - pcb);
	}

error:
	return 0.0;
} // g_zM()


/** \brief \f$ \frac{d\log(g_zM(z,M))}{d\log M} \f$
 *
 * Function describing the logarithmic deriavtive of \f$ g_{zM} \f$
 * with respect to M
 */
double
d_lngzM_d_lnM(const double z,		/**< [in] Redshift. */
	      const double M		/**< [in] Mass Svale. */)
{
	check(z >= 0, "Redshift of %.2f is not in expected range.", z);
	check(M > 0, "Mass must be positive-definite.");

	if (theta_g->omega_nu_0 == 0.0)
	{
		return 0;
	}
	else
	{
		double fcb = 1. - (theta_g->omega_nu_0 / theta_g->omega_M_0);
		double yfs = y_fs(M); // free streaming scale for neutrinos
		double pcb = p_cb(); // describes how growth is suppressed by neutrinos
		double gz  = g_z(z); // scale-independent growth at z
		double g0  = g_z(0.); // scale-independent growth at z=0
		double dz = pow(gz, 0.7) / ( pow(fcb, 0.7 / pcb) + pow(gz / (1. + yfs), 0.7) );
		double d0 = pow(g0, 0.7) / ( pow(fcb, 0.7 / pcb) + pow(g0 / (1. + yfs), 0.7) );
		double dlngzM_dlnM = ( 2. * yfs * pcb / (3. * pow(1. + yfs, 1.7)) );
		return dlngzM_dlnM * (dz - d0);
	}

error:
	return 0.0;
} // d_lngzM_d_lnM()


/** \brief \f$ g(z) \f$
 *
 * Function for scale independent growth
 * from version shown in Eisenstein and Hu (1997), cited
 * there as being from Lahav et al. 1992, Carroll et al. 1992.
 */
double
g_z(const double z		/**< [in] Redshift. */)
{
	check(z >= 0, "Redshift of %.2f is not in expected range.", z);

	double omz = omega_M(z);
	double olz = omega_lambda(z);
	double den = pow(omz, 4./7.) - olz + (1. + omz / 2.) * (1. + olz / 70.);
	den = 2. * (1. + z) * den;
	double norm = 1. + Z_EQ * theta_g->omega_M_0 * theta_g->h0 * theta_g->h0;
	double res = norm * 5. * omz / den;
	return res;
	
error:
	return 0.0;
} // g_z()


/** \brief \f$ p_{cb} \f$
 *
 * Function for getting \f$ p_{cb} \f$, the neutrino growth suppression factor.
 */
double
p_cb()

{
	double fnu = theta_g->omega_nu_0 / theta_g->omega_M_0;
	double fcb = 1. - fnu;
	return (5. - sqrt(1. + 24. * fcb)) / 4.;
} // p_cb()


/** \brief \f$ y_{fs} \f$
 *
 * Function for calculating the neutrino free-streaming scale.
 */
double
y_fs(const double M		/**< [in] Mass. */)
{
	check(M > 0, "Mass must be positive-definite.");

	double fnu = theta_g->omega_nu_0 / theta_g->omega_M_0;
	double q = q_M(M);
	double yfs = 17.2 * fnu * (1. + 0.488 * pow(fnu, -7./6.));
	yfs = yfs * pow(N_NU / fnu, 2);
	return yfs * q * q;

error:
	return 0.0;
} // y_fs()


/** \brief \f$ q(M) \f$
 *
 * Function for calculating \f$ q \f$, a unitless variable proportional to wavenumber.
 */
double
q_M(const double M 		/**< [in] Mass. */)
{
	check(M > 0, "Mass must be positive-definite.");


	double omhh = theta_g->omega_M_0 * theta_g->h0 * theta_g->h0;
	double k = k_M(M);
	return k * THETA_CMB * THETA_CMB * theta_g->h0 / omhh;

error:
	return 0.0;
} // q_M()


/** \brief \f$ k(M) \f$
 *
 * Function to relate a mass in \f$ M_{sun} \cdot h^{-1} \f$ to a 
 * wavenumber in \f$ h \cdot \text{Mpc}^{-1} \f$. Returns \f$ k \f$ so that
 * \f$ k^{-1} \f$ is the radius of a sphere containing mass \f$ M \f$.
 */
double
k_M(const double M		/**< [in] Mass. */)
{
	check(M > 0, "Mass must be positive-definite.");

	double k = 0.125 * pow(M_8 * theta_g->omega_M_0 * M_SCALE/M, 1./3.);
	return k;

error:
	return 0.0;
} // k_M()


/** \brief Shape-governing function \f$ \sigma(M) \f$
 * save for a normalization factor (fit function).
 */
double
ln_inv_sigma_Evrard(const double M,	/**< [in] Mass. */
		    const double z	/**< [in] Redshift. */)
{
	check(M > 0, "Mass must be positive-definite.");
	check(z >= 0, "Redshift of %.2f is not in expected range.", z);

	double ln_15 = -log( 0.578 * ( theta_g->sigma_8 / 0.9 ) );
	double a     = 0.281;
	double b     = 0.0123;
	double x     = log(M/1e15);
	double g     = g_linder(z) / g_linder(0.0);
	double ln_g  = -1 * log(g);
	return ln_15 + (a * x) + (b * x * x) + ln_g;

error:
	return 0.0;
} // ln_inv_sigma_Evrard()


/** \brief \f$ \alpha_{eff}^{(EV)} \f$
 * Based on constants from Evrard et al. (2002)
 */
double
alpha_eff_Evrard(const double M		/**< [in] Mass. */)
{
	check(M > 0, "Mass must be positive-definite.");

	double a = 0.281;
	double b = 0.0123;
	double x = log(M / 1e15);
	return a + (2 * b * x);

error:
	return 0.0;
} // alpha_eff_Evrard()


/** Mass Fraction
 *
 * Function uses the function \f$ \sigma(M) \f$ to compute the
 * probability that a unit of matter of mass \f$M\f$ has formed
 * a virialized halo.
 * 
 * It is currently based on a version of the Jenkins mass
 * function for spherical halos that encompass a mean density
 * 200 times critical, with coefficients from Evrard et al. (2002).
 * \param ln_inv_sigma result of ln[&sigma;(M)<sup>-1</sup>]
 */
double
mass_fraction(const double ln_inv_sigma		/**< [in] Result of \f$\log(\sigma(M)^{-1})\f$. */)
{
	//TODO: Add valid range check.
	double a = 0.22;
	double b = 0.73;
	double eps = 3.86;
	return a * exp(-1 * pow(fabs(ln_inv_sigma + b), eps));
} // mass_fraction()


/** \f$ \frac{\partial^2 N}{\partial V\partial\mu}(M, z) \f$
 *
 * Function converts the mass fraction into a differential
 * number density of halos of mass M at redshift \f$z\f$ per unit \f$\mu\f$, 
 * where \f$\mu = \log_{10}(M)\f$. Number density units are 
 * \f$ (h\cdot\text{Mpc}^{-1})^3 \f$ and the units of \f$M\f$ are
 * \f$h^{-1}\cdot M_{sun}\f$.
 */
double
dN_dV_dmu(const double M,			/**< [in] Mass. */
	  const double z			/**< [in] Redshift. */)
{
	check(M > 0, "Mass must be positive-definite.");
	check(z >= 0, "Redshift of %.2f is not in expected range.", z);

	double dndvdmu = (omega_M(z) * RHO_CRIT / M) * alpha_eff(M, z) * mass_fraction(ln_inv_sigma(M, z)) * log(10.0);
	return dndvdmu;

error:
	return 0.0;
} // dN_dV_dmu()


/** \brief \f$ \lambda(\mu,z)\f$
 *
 * Function to calculate richness parameter \f$\lambda\f$ as
 * a function of mass \f$\mu\f$ and redshift \f$z\f$. Current
 * version is taken from Rykoff et al. (2011).
 */
double
lambda_of_mu(const double mu, 			/**< [in] Mass Scale \f$\log_{10}(M)\f$. */
	     const double z			/**< [in] Redshift. */)
{
	check(mu >= 0, "Mass Scale must be positive-semidefinite.");
	check(z >= 0, "Redshift of %.2f is not in expected range.", z);

	double M_0 = 0.7 * pow(theta_g->h0, -1.) * 1e14 * exp(1.48);
	return theta_g->lambda_0 * pow(pow(10., mu) / M_0, 1. / 1.06) * pow(E(z), theta_g->beta_lambda);

error:
	return 0.0;
} // lambda_of_mu()


/** \brief \f$\mu(\lambda,z)\f$ 
 *
 * Inverse of \f$\lambda(\mu,z)\f$.
 */
double
mu_of_lambda(const double lambda, 		/**< [in] Richness \f$\lambda\f$. */
	     const double z			/**< [in] Redshift. */)
{
	check(lambda > 0, "Richness must be positive-definite.");
	check(z >= 0, "Redshift of %.2f is not in expected range.", z);

	double M_0 = 0.7 * (1. / theta_g->h0) * 1e14 * exp(1.48);
	double mu = log10(M_0 * pow( (lambda / theta_g->lambda_0) * pow(E(z), -1 * theta_g->beta_lambda), 1.06));
	return mu;

error:
	return 0.0;
} // mu_of_lambda()
