#ifndef STRUCTURE_H // include guard
#define STRUCTURE_H

double
integrand_linder(double z, 
		 void* argv);

double
g_linder(double z);

double
ln_inv_sigma(double M,
	     double z);

double
alpha_eff(double M,
	  double z);

double
ln_sigma_z0(double M);

double
d_lnsigz0_d_lnM(double M);

double
lnM_M8norm(double M);

double
lnsig_z0_lincoeff();

double
lnsig_z0_quadcoeff();

double
lnsig_z0_thirdcoeff();

double
g_zM(double z,
     double M);

double
d_lngzM_d_lnM(double z,
	      double M);

double
g_z(double z);

double
p_cb();

double
y_fs(double M);

double
q_M(double M);

double
k_M(double M);

double
ln_inv_sigma_Evrard(double M,
		    double z);

double
alpha_eff_Evrard(double M);

double
mass_fraction(double ln_inv_sigma);

double
dN_dV_dmu(double M,
	  double z);

double
lambda_of_mu(double mu,
	     double z);

double
mu_of_lambda(double l,
	     double z);

#endif // include guard
