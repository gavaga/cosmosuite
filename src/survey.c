#include "survey.h"

#include <stdlib.h>
#include <math.h>

#include "constants.h"
#include "types.h"
#include "utils.h"
#include "cosmology.h"
#include "structure.h"
#include "quadrature.h"
#include "debug.h"


/** \brief scale
 *
 * This parameter dictates the number of standard deviations the survey covers
 */
const double scale = 3.0;


/** \brief \f$\theta_g\f$
 *
 * Global struct of cosmological parameters set in gen_N to be used throughout the computation.
 */
extern CParams * theta_g;


/** \brief Probability density function
 * \f$P(\lambda|\mu,z)\f$
 * Probability that a galaxy cluster has richness \f$\lambda\f$, given its mass scale
 * \f$\mu=\log_{10}(M)\f$ and redshift \f$z\f$.
 *
 * \changelog Modified on 2013-07-17 by GMM to be a probability density function 
 * for optical richness of cluster with a given mass \f$M = 10.0^\mu\f$ in
 * \f$M_{sun}\f$. The form of the PDF is Gaussian in richness at fixed z.
 */
double
pdf(const double logl,			/**< [in] Richness \f$\log \lambda\f$. */
    const double mu,			/**< [in] Mass Scale. */
    const double z			/**< [in] Redshift. */)
{
	check(logl >= 0, "Log-Richness must be positive-semidefinite.");
	check(mu >= 0, "Mass Scale must be positive-semidefinite.");
	check(z > 0, "Redshift must be positive definite.");

	double exp_lambda = lambda_of_mu(mu, z);
	exp_lambda = log10(exp_lambda);

	double p = 1. / (sqrt(2. * PI) * theta_g->sig_lambda) * exp(-0.5 * pow(logl - exp_lambda,2) / pow(theta_g->sig_lambda, 2));
	return p;

error:
	return 0.0;
}


/** \brief \f$\frac{dN}{dV\,d\lambda\,d\mu}\f$
 *
 * This function calculates the number density per unit log(M) per unit log(&lambda;).
 *
 * GMV (1/4/11): This [function] is the integrand over which [Cuba's 
 * Cuhre algorithm] performs a 2-dimensional integration, and that integration
 * is done over a unit hypercube whose coordinates are represented by the vector xx, which
 * has ndim=2 dimensions. The integrand therefore must be properly normalized through the
 * chain-rule factors [dl_du1] and dmu_du2.
 *
 * \changelog 2013-07-18 GMM: Changed function to operate in optical richness.
 */
int
dN_dV_dl_dmu(const int* ndim,		/**< [in] Number of dimensions to integrate; in this case, \f$ndim=2\f$. */
	     const double xx[],		/**< [in] Vector of coordinates passed by Cuhre. */
	     const int* ncomp,		/**< [in] Number of output dimensions. */
	     double ff[],		/**< [out] Vector of results. */
	     void* userdata		/**< [out] Vector \f$[z, \log\lambda_0, \log\lambda_1]\f$. */)
{
	(void) ndim; (void) ncomp;

	double* params = (double *)userdata; //z, l1, l2
	double z = params[0];
	double logl1 = params[1];
	double logl2 = params[2];

	check(z > 0, "Redshift must be positive definite.");
	check(logl1 >= 0 && logl2 >= 0, "Richness must be positive semidefinite.");
	check(logl2 > logl1, "Invalid richness range, l1 > l2.");

	double dlogl = logl2 - logl1;
	double logl = xx[0] * dlogl + logl1; 
	double dl_du1 = dlogl;

	double mu1 = mu_of_lambda(pow(10., logl - scale * theta_g->sig_lambda), z);
	double mu2 = mu_of_lambda(pow(10., logl + scale * theta_g->sig_lambda), z);

	double dmu = mu2 - mu1;
	double mu = xx[1] * dmu + mu1;
	double dmu_du2 = dmu;
	double prob = pdf(logl, mu, z);
	double dN_dV_dmu_v = dN_dV_dmu(pow(10., mu), z);


	ff[0] = dN_dV_dmu_v * prob * dl_du1 * dmu_du2;
	return 0;

error:
	ff[0] = 0.0;
	return 0;
}


/** \brief \f$\frac{dN}{dV}\f$
 *
 * Number per comoving volume (number density). Calls the Cuba library's
 * Cuhre multi-dimensional integration routine with dN_dv_dl_dmu as its
 * integrand.
 * \units \f$(h\cdot\text{Mpc}^{-1})^3\f$
 */
double
dN_dV(const double z,			/**< [in] Redshift. */
      const double l1,			/**< [in] Richness lower bound. */
      const double l2			/**< [in] Richness upper bound. */)
{
	double * params = NULL;

	check(z > 0, "Redshift must be positive definite.");
	check(l1 > 0 && l2 > 0, "Richness must be positive definite.");
	check(l2 > l1, "Invalid richness range, l1 > l2.");

	double val, err;
	int neval;
	params = (double *)malloc(3 * sizeof(double));
	check_mem(params);

	params[0] = z;
	params[1] = log10(l1);
	params[2] = log10(l2);

	cuba_cuhre(&dN_dV_dl_dmu, 2, 1e-3, (void *)params, &val, &err, &neval);
	free(params);
	return val;

error:
	if (params)
		free(params);
	return 0.0;
}


/** \brief \f$\frac{d^2N}{d\Omega dz}\f$
 *
 * This function calculates the number per solid angle per unit redshift.
 */
double
dN_dOmega_dz(double z,			/**< [in] Redshift. */
	     void* argv			/**< [in] Vector \f$[\lambda_1, \lambda_2]\f$. */)
{
	check(z > 0, "Redshift must be positive definite.");

	double* params = (double *)argv;
	double l1 = params[0];
	double l2 = params[1];
	double dNdodz = dV_dOmega_dz(z) * dN_dV(z, l1, l2) * pow(C / H100,3);
	return dNdodz;

error:
	return 0.0;
}


/** \brief \f$\frac{dN}{d\Omega}\f$
 * Number (of clusters) per solid angle.
 */
double
dN_dOmega(const double z1,		/**< [in] Redshift lower bound. */
	  const double z2,		/**< [in] Redshift upper bound. */
	  const double l1,		/**< [in] Richness lower bound. */
	  const double l2		/**< [in] Richness upper bound. */)
{
	double * params = NULL;

	check(z1 > 0 && z2 > 0, "Redshift must be positive definite.");
	check(l1 > 0 && l2 > 0, "Richness must be positive definite.");
	check(z2 > z1, "Invalid redshift range, [%.2f,%.2f].", z1, z2);
	check(l2 > l1, "Invalid richness range, [%.2f,%.2f].", l1, l2);

	params = (double *) malloc(2 * sizeof(double));
	check_mem(params);

	params[0] = l1;
	params[1] = l2;
	double res = gsl_qag_extra(&dN_dOmega_dz, z1, z2, (void *)params);
	free(params);
	return res;
	
error:
	if (params)
		free(params);
	return 0.0;
}


/** \brief \f$dN\f$
 *
 * Number of clusters in a given richness-redshift bin.
 */
double
dN(const double z1,			/**< [in] Redshift lower bound. */
   const double z2,			/**< [in] Redshift upper bound. */
   const double l1,			/**< [in] Richness lower bound. */
   const double l2,			/**< [in] Richness upper bound. */
   const double dOmega			/**< [in] Survey solid angle \f$d\Omega\f$. */)
{
	check(z1 > 0 && z2 > 0, "Redshift must be positive definite.");
	check(l1 > 0 && l2 > 0, "Richness must be positive definite.");
	check(z2 > z1, "Invalid redshift range, [%.2f,%.2f].", z1, z2);
	check(l2 > l1, "Invalid richness range, [%.2f,%.2f].", l1, l2);
	check(dOmega > 0, "Solid angle must be positive definite.");

	double d = dN_dOmega(z1, z2, l1, l2) * dOmega;
	return d;

error:
	return 0.0;
}


/** \brief Survey
 *
 * Function generates a matrix of bins in richness and redshift
 * with predictions as to what cluster counts a survey with
 * the given bounds on richness and redshift measurement and with
 * a survey solid angle of \f$d\Omega\f$ would expect to detect.
 */
CMatrix *
gen_N(CParams * theta,			/**< [in] Cosmological Parameters. */
      SParams * survey,			/**< [in] Survey Parameters. */
      Binning ** binnings		/**< [in] Array of binnings for Redshift, Richness, etc. */)
{
	check(theta, "No cosmological parameters provided.");
	check(survey, "No survey parameters provided.");
	check(binnings, "No binnings provided.");

	theta_g = theta;

	size_t total_zbins = calc_total_bins(binnings[0]);
	size_t total_lbins = calc_total_bins(binnings[1]);

	double dOmega = survey->dOmega;

	double dz, z1, z2, dl, l1, l2;

	CMatrix * N = CMatrix_make(total_zbins, total_lbins);
	check(N, "Failed to allocate matrix.");

	size_t zdx = 0;
	size_t ldx = 0;
	for (size_t i = 0; i < binnings[0]->nsegs; ++i)
	{
		dz = (binnings[0]->bounds[i + 1] - binnings[0]->bounds[i]) / (double)binnings[0]->subdvs[i];
		for (size_t j = 0; j < binnings[0]->subdvs[i]; ++j)
		{
			z1 = binnings[0]->bounds[i] + (double)j * dz;
			z2 = binnings[0]->bounds[i] + (double)(j + 1) * dz;

//			printf("z: [%6.2f, %6.2f]", z1, z2);

			for (size_t k = 0; k < binnings[1]->nsegs; ++k)
			{
				dl = (binnings[1]->bounds[k + 1] - binnings[1]->bounds[k]) / (double)binnings[1]->subdvs[k];
				
				for (size_t l = 0; l < binnings[1]->subdvs[k]; ++l)
				{
					l1 = binnings[1]->bounds[k] + (double)l * dl;
					l2 = binnings[1]->bounds[k] + (double)(l + 1) * dl;

//					printf(" l: [%6.2f, %6.2f]", l1, l2);

					CMatrix_set(N, zdx, ldx, dN(z1, z2, l1, l2, dOmega));
					
					++ldx;
				}
			}

//			printf("\n");

			++zdx;
			ldx = 0;
		}
	}
	return N;

error:
	if (N)
		CMatrix_destroy(N);
	return NULL;
}

//double n_bar_of_z(const double z)
//{
//	//TODO: replace 10.0 with rmin
//	double mu_min = structure::lambda_mu::mu_of_lambda(10.0, z);
//	double zed = z;
//	
//	cout << "Calling qagi; mu_min: " << mu_min << endl;
//	return utils::quadrature::gsl::qagi(&spc::dN_dV_dmu_int, pow(10, mu_min), 1, static_cast<void*>(&zed));
//}
//
//double prob_m(const double M,
//	      const double l,
//	      const double z)
//{
//	double norm = 1.;
//	return norm * spc::dN_dM(M, z) * cluster::pdf(l, log10(M), z);
//}
//
//double b_eff_int(double M,
//		 void* argv)
//{
//	double* params = static_cast<double*>(argv);
//	double l = params[0];
//	double z = params[1];
//
//	return spc::linear_bias(M, z) * prob_m(M, l, z);
//}
//
//double b_eff(double l,
//	     void* argv)
//{
//	double* params = static_cast<double*>(argv);
//
//	double z = params[0];
//	double r_min = params[1];
//
//	params[0] = l;
//	params[1] = z;
//	return utils::quadrature::gsl::qagi(&b_eff_int, pow(10, structure::lambda_mu::mu_of_lambda(r_min, z)), 1, static_cast<void*>(const_cast<double*>(params)));
//}
//
//double b_eff_ave(const double z,
//		 const double l1, 
//		 const double l2)
//{
//	double* params = new double[2];
//	params[0] = z;
//	params[1] = l1;
//	double res = utils::quadrature::gsl::qag(&b_eff, l1, l2, 10, static_cast<void*>(params));
//	delete[] params;
//	return res;
//}
//
//double dV_dz(const double z,
//	     const double dOmega)
//{
//	return cosmology::dV_dOmega_dz(z, static_cast<void*>(NULL)) * dOmega;
//}
//
//double p_cl(const double k,
//	    const double z,
//	    const double l1,
//	    const double l2)
//{
//	double b_eff2 = pow(b_eff_ave(z, l1, l2), 2);
//	double p_k_z = spm::p_of_k_z(k, z);
//	return b_eff2 * p_k_z;
//}
//
//double p_bar_int_num(double z,
//		     void* argv)
//{
//	double* params = static_cast<double*>(argv);
//
//	double k = params[0];
//	double l1 = params[1];
//	double l2 = params[2];
//	double dOmega = params[3];
//
//	return p_bar_int_den(z, static_cast<void*>(&dOmega)) * p_cl(k, z, l1, l2);
//}
//
//double p_bar_int_den(double z,
//		     void* argv)
//{
//	double dOmega = *(static_cast<double*>(argv));
//
//	double int_den = dV_dz(z, dOmega);
//	return int_den * pow(n_bar_of_z(z), 2);
//}
//
//double p_bar_cl(double k,
//		void* argv)
//{
//	double* params = static_cast<double*>(argv);
//
//	double z1 = params[0];
//	double z2 = params[1];
//	double l1 = params[2];
//	double l2 = params[3];
//	double dOmega = params[4];
//
//	params = new double[4];
//	params[0] = k;
//	params[1] = l1;
//	params[2] = l2;
//	params[3] = dOmega;
//
//	double p_bar_cl = utils::quadrature::gsl::qag(&p_bar_int_num, z1, z2, 10, static_cast<void*>(params));
//	delete[] params;
//
//	return p_bar_cl / utils::quadrature::gsl::qag(&p_bar_int_den, z1, z2, 10, static_cast<void*>(&dOmega));
//}
//
//double p_bar_cl_ave(const double k,
//		    const double dk,
//		    const double z1,
//		    const double z2,
//		    const double l1,
//		    const double l2,
//		    const double dOmega)
//{
//	double* params = new double[5];
//	params[0] = z1;
//	params[1] = z2;
//	params[2] = l1;
//	params[3] = l2;
//	params[4] = dOmega;
//
//	double result = utils::quadrature::gsl::qag(&p_bar_cl, k, k * pow(10, dk), 10, static_cast<void*>(params)) / dk;
//	delete[] params;
//	return result;
//}
//
//double v_n(const double k,
//	   const double dk)
//{
//	return pow(k, 2) * dk / (2. * pow(4 * constants::pi, 2));
//}
//
//double v_eff_int(double z,
//		 void* argv)
//{
//	double* params = static_cast<double*>(argv);
//
//	double k = params[0];
//	double l1 = params[1];
//	double l2 = params[2];
//	
//	double factor = n_bar_of_z(z) * p_cl(k, z, l1, l2);
//	return (factor / (1. + factor)) * cosmology::dV_dOmega_dz(z, argv);
//}
//
//double v_eff(const double k, 
//	     const double l1,
//	     const double l2,
//	     const double dOmega)
//{
//	double* params = new double[3];
//
//	params[0] = k;
//	params[1] = l1;
//	params[2] = l2;
//
//	double res = dOmega * utils::quadrature::gsl::qagi(&v_eff_int, 0., 1, static_cast<void*>(params));
//	delete[] params;
//	return res;
//}
//
//double sigma2_p_cl(const double p_cl,
//		   const double k,
//		   const double dk,
//		   const double l1,
//		   const double l2,
//		   const double dOmega)
//{
//	return 2. * p_cl*p_cl / (v_n(k, dk) * v_eff(k, l1, l2, dOmega));
//}
//
//boost::numeric::ublas::matrix<double> gen_P(struct cosmology::cosmoparams theta,
//			  const int nk,
//			  const double z1,
//			  const double z2,
//			  const double k1,
//			  const double k2,
//			  const double l1,
//			  const double l2,
//			  const double dOmega)
//{
//	theta_g = theta;
//
//	double dk = log(k2/k1) / static_cast<double>(nk);
//
//	boost::numeric::ublas::matrix<double> p (nk, 2);
//
//	for (int i = 0; i < nk; ++i)
//	{
//		double p_cl = p_bar_cl_ave(k1 * pow(10, (((i/2) + 1) * dk)), dk, z1, z2, l1, l2, dOmega);
//		p(i,0) = p_cl;
//		p(i,1) = sqrt(error::sigma2_p_cl(p_cl, k1, dk, l1, l2, dOmega));
//	}
//	return p;
//}
