#ifndef SURVEY_H // include guard
#define SURVEY_H

#include "types.h"

extern const double scale;

double
pdf(const double l,
    const double mu,
    const double z);

int
dN_dv_dl_dmu(const int* ndim,
	     const double xx[],
	     const int* ncomp,
	     double ff[],
	     void* userdata);

double
dN_dv(const double z,
      const double l1,
      const double l2);

double
dN_dOmega_dz(double z,
	     void* argv);

double
dN_dOmega(const double z1,
	  const double z2,
	  const double l1,
	  const double l2);

double
dN(const double z1, 
   const double z2,
   const double l1,
   const double l2,
   const double dOmega);

CMatrix * 
gen_N(CParams * theta,
      SParams * survey,
      Binning ** binnings);

///**
// * Set of functions for calculating the cluster power spectrum P<sub>cl</sub>(k).
// *
// * \author GMM
// * \date 2013-07-**
// */
//namespace power_spectrum
//{
//
///** \brief Selection function
// * which calculates the average number density of detected clusters in a survey.
// * \param z redshift
// */
//double
//n_bar_of_z(const double z);
//
///** \brief P(M|&lambda;,z)
// * Function to calculate the probability of a mass M given the richness l 
// * at a redshift z.
// * \param M mass scale
// * \param l richness
// * \param z redshift
// */
//double
//prob_m(const double M,
//	      const double l,
//	      const double z);
//
///**
// * Integrand for the calculation of the average bias at a given
// * richness and redshift.
// * \param M mass scale
// * \param argv void* containing fixed &lambda; and z.
// */
//double
//b_eff_int(double M,
//		 void* argv);
//
///** \brief Effective bias
// * Function which calculates the effective bias factor.
// * \param l richness parameter &lambda;
// * \param argv void* containing redshift z and minimum richness &lambda;<sub>min</sub> (maybe)
// */
//double
//b_eff(double l,
//	     void* argv);
//
///**
// * Function which averages the effective bias over richness
// * \param z redshift
// * \param l1 lower richness bound
// * \param l2 upper richness bound
// */
//double
//b_eff_ave(const double z,
//		 const double l1,
//		 const double l2);
//
///** \brief dV/dz
// * Function which calculates the derivate of the comoving volume element
// * with respect to redshift.
// * \param z redshift
// * \param dOmega survey solid angle d&Omega;
// */
//double
//dv_dz(const double z,
//	     const double dOmega);
//
///** \brief P<sub>cl</sub>
// * Function which calculates the cluster power spectrum.
// * Adapted from Majumdar & Mohr (2004) via JLM (2011).
// * \param k scale factor
// * \param z redshift
// * \param l1 lower richness bound
// * \param l2 upper richness bound
// */
//double
//p_cl(const double k,
//	    const double z,
//	    const double l1,
//	    const double l2);
//
///**
// * Integrand for the numerator of p_bar_cl()
// * \param z redshift
// * \param argv void* containing k, l1, l2, and dOmega
// */
//double
//p_bar_int_num(double z,
//		     void* argv);
//
///**
// * Integrand for the denominator of p_bar_cl()
// * \param z redshift
// * \param argv void* containting dOmega
// */
//double
//p_bar_int_den(double z,
//		     void* argv);
//
///** \brief P<sub>cl</sub>(k)
// * Calculates the redshift-averaged cluster power spectrum over a redhisft
// * inverval [z1, z2] with a survey solid angle d&Omega;.
// * \param k scale factor
// * \param argv void* containing redshift bounds z1, z2; richness bounds l1, l2; and survey solid angle d&Omega;
// */
//double
//p_bar_cl(double k,
//		void* argv);
//
///** 
// * Averages P_bar_cl(k) over the k-bin defined by the interval [k, k*10<sup>dk</sup>]
// * \param k multipole moment k
// * \param dk logarithmic k-bin width
// * \param z1 lower redshift bound
// * \param z2 upper redshift bound
// * \param l1 lower richness bound
// * \param l2 upper richness bound
// * \param dOmega survey solid angle
// */
//double
//p_bar_cl_ave(const double k,
//		    const double dk,
//		    const double z1,
//		    const double z2,
//		    const double l1,
//		    const double l2,
//		    const double dOmega);
///**
// * Set of functions relating to the calculation of error in the 
// * P(k) calculation.
// *
// * \author GMM
// * \date 2013-07-**
// */
//namespace error
//{
//
///** \brief Volume
// * of the shell in k-space that defines the k-bin starting at k with 
// * logarithmic width dk.
// * \param k multipole moment
// * \param dk logarithmic width of k-bin
// */
//double
//v_n(const double k,
//	   const double dk);
//
///**
// * Integrand for calculating effective survey volume.
// * \param z redshift
// * \param argv void* containting k, l1, and l2.
// */
//double
//v_eff_int(double z,
//		 void* argv);
//
///** \brief Survey Volume
// * where <code>P(k)</code> is larger than the shot noise.
// * Effective survey volume.
// * \param k multipole moment
// * \param l1 richness lower bound
// * \param l2 richness upper bound
// * \param dOmega survey solid angle
// */
//double
//v_eff(const double k,
//	     const double l1,
//	     const double l2,
//	     const double dOmega);
//
///** \brief Estimated uncertainty
// * in P<sub>cl</sub>(k<sub>n</sub>); variance.
// * \param p_cl cluster power spectrum magnitude for the k-bin defined by k and dk
// * \param k multipole moment
// * \param dk logarithmic width in k-space of k-bin n
// * \param l1 richness lower bound
// * \param l2 richness upper bound
// * \param dOmega survey solid angle
// */
//double
//sigma2_p_cl(const double p_cl,
//		   const double k,
//		   const double dk,
//		   const double l1,
//		   const double l2,
//		   const double dOmega);
//
//} // namespace error
//
///** \brief Cluster power spectrum
// * calculation which returns a nkx2 matrix with
// * the first column representing the cluster power spectrum
// * and the second the associated estimation of error for each
// * average cluster power spectrum value.
// * \param theta struct containing command-line defined cosmological parameters
// * \param z1 redshift lower bound
// * \param z2 redshift upper bound
// * \param k1 multipole moment lower bound
// * \param k2 multipole moment upper bound
// * \param nk number of wavenumber bins
// * \param l1 richness lower bound
// * \param l2 richness upper bound
// * \param dOmega survey solid angle 
// */
//boost::numeric::ublas::matrix<double> gen_P(struct cosmology::cosmoparams theta,
//					    const int nk = 5,
//					    const double z1 = 0.1,
//					    const double z2 = 1.5,
//					    const double k1 = 0.0014,	
//    					    const double k2 = 1.4,	
//    					    const double l1 = 10,	
//    					    const double l2 = 350,	
//    					    const double dOmega = 12.0);
//
//

#endif // include guard
