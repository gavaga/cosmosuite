#include "types.h"

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdint.h>

#include "constants.h"
#include "debug.h"


/** \brief CParams allocator
 *
 * Function allocates space for a CParams struct and returns a pointer to it.
 * Values are not initialized, so it is recommended that this function is only used
 * in contexts where all values are initialized immediately. For example, see
 * lcdm() in cosmology.c.
 * \sa lcdm
 */
CParams *
CParams_make()
{
	CParams * p = (CParams *) calloc(1, sizeof(CParams));
	check_mem(p);
	return p;

error:
	if (p)
		CParams_destroy(p);
	return NULL;
}


/** \brief CParams freer
 *
 * Function which frees the space occupied by a CParams object.
 */
void
CParams_destroy(CParams * theta		/**< [in] Pointer to object to be deallocated. */)
{
	if (theta)
		free(theta);
}


/** \brief CTensor allocator
 */
CTensor *
CTensor_make(const size_t ndim,		/**< [in] Number of dimensions. */
	     size_t * sizes		/**< [in] Array containing length of tensor in each dimension. */)
{
	CTensor * T = NULL;

	check(ndim > 0, "Invalid number of dimensions provided.");
	check(sizes, "Invalid size list provided.");

	T = (CTensor *)malloc(sizeof(CTensor));
	check_mem(T);

	T->ndim = ndim;
	T->dims = (size_t *)malloc(T->ndim * sizeof(size_t));
	check_mem(T->dims);

	memcpy(T->dims, sizes, T->ndim * sizeof(size_t));

	size_t prod = 1;
	for (size_t i = 0; i < ndim; ++i)
	{
		prod *= sizes[i];
	}
	
	T->data = (double *)calloc(prod, sizeof(double));
	check_mem(T->data);

	return T;

error:
	if (T)
		CTensor_destroy(T);
	return NULL;
}


/** \brief CTensor copy constructor
 *
 * Deep copy to create an identical CTensor to the one pointed to by T.
 */
CTensor *
CTensor_copy(const CTensor * T		/**< [in] CTensor to deep-copy. */)
{
	CTensor * K = CTensor_make(T->ndim, T->dims);

	size_t size = 1;

	for (size_t i = 0; i < T->ndim; ++i)
	{
		size *= K->dims[i];
	}

	memcpy(K->data, T->data, size * sizeof(double));
	return K;
}


/** \brief CTensor freer
*/
void
CTensor_destroy(CTensor * T		/**< [in] Pointer to object to be destroyed. */)
{
	if (T)
	{
		if (T->dims)
			free(T->dims);
		if (T->data)
			free(T->data);
		free(T);
	}
}


/** \brief CTensor getter
 */
double
CTensor_get(const CTensor * T,		/**< [in] Pointer to object to access. */
	    ...				/**< [in] Indices. */)
{
	check(T, "Can't GET from NULL pointer.");

	va_list ap;
	va_start(ap, T);

	size_t index = 0;
	for (size_t i = 0; i < T->ndim; ++i)
	{
		size_t dstride = 1;
		for (size_t q = 0; q < i; ++q)
		{
			dstride *= T->dims[q];
		}

		index += (va_arg(ap, size_t) * dstride);
	}
	va_end(ap);

	return T->data[index];

error:
	return 0.0;
}


/** \brief CTensor setter
 */
void
CTensor_set(CTensor * T,		/**< [in] Pointer to object to modify. */
	    double val,			/**< [in] New value. */
	    ...				/**< [in] Indices. */)
{
	check(T, "Can't set NULL pointer.");

	va_list ap;
	va_start(ap, val);

	size_t index = 0;
	for (size_t i = 0; i < T->ndim; ++i)
	{
		size_t dstride = 1;
		for (size_t q = 0; q < i; ++q)
		{
			dstride *= T->dims[q];
		}

		index += (va_arg(ap, size_t) * dstride);
	}
	va_end(ap);

	T->data[index] = val;
	return;

error:
	return;
}


/** \brief CTensor update function
 */
void
CTensor_update(CTensor * T,		/**< [in] Pointer to object to modify. */
	       double (*func)(double),	/**< [in] Update function. */
	       ...			/**< [in] Indices. */)
{
	check(T, "Can't update NULL CTensor.");
	check(func, "No update function provided.");

	va_list ap;
	va_start(ap, func);

	size_t index = 0;
	for (size_t i = 0; i < T->ndim; ++i)
	{
		size_t dstride = 1;
		for (size_t q = 0; q < i; ++q)
		{
			dstride *= T->dims[q];
		}

		index += (va_arg(ap, size_t) * dstride);
	}
	va_end(ap);

	T->data[index] = func(T->data[index]);
	return;

error:
	return;
}


/** \brief CMatrix allocator
 *
 * Function allocates space for a CMatrix struct of size \f$n_r\times n_c\f$.
 */
CMatrix *
CMatrix_make(const size_t nr,		/**< [in] Number of rows. */
	     const size_t nc		/**< [in] Number of columns. */)
{
	CMatrix * mat = malloc(sizeof(CMatrix));
	check_mem(mat)

	mat->nr = nr;
	mat->nc = nc;
	mat->data = (double *)calloc(nr*nc, sizeof(double));
	check_mem(mat->data);
	return mat;

error:
	if (mat)
		CMatrix_destroy(mat);
	return NULL;
}


/** \brief CMatrix copy constructor
 *
 * Performs a deep copy of M to construct a new CMatrix.
 */
CMatrix *
CMatrix_copy(const CMatrix * M		/**< [in] CMatrix to copy. */)
{
	check(M, "No valid CMatrix provided.");

	CMatrix * A = CMatrix_make(M->nr, M->nc);
	memcpy(A->data, M->data, M->nr * M->nc * sizeof(double));
	return A;

error:
	return NULL;
}


/** \brief CMatrix freer
 *
 * Function which frees space previously allocated to a CMatrix object.
 */
void
CMatrix_destroy(CMatrix * mat		/**< [in] Pointer to object to be deallocated. */)
{
	if (mat)
	{
		if (mat->data)
			free(mat->data);
		free(mat);
	}
}


/** \brief CMatrix element accessor
 *
 * Function which accesses the element in the jth row of the ith column of the matrix.
 */
double
CMatrix_get(const CMatrix * mat,	/**< [in] Matrix whose element is to be accessed. */
	    const size_t i,		/**< [in] Row index. */
	    const size_t j			/**< [in] Column index. */)
{
	check(mat, "No valid CMatrix provided.");
	check(i < mat->nr && j < mat->nc, "Index %zd,%zd out of bounds.", i, j);

	return mat->data[i*mat->nc + j];

error:
	return 0.0;
}

/** \brief CMatrix element setter
 */
void
CMatrix_set(CMatrix * mat, 		/**< [in] Matrix. */
	    const size_t i,		/**< [in] Row index. */
	    const size_t j,		/**< [in] Col index. */
	    const double v		/**< [in] New value. */)
{
	check(mat, "No valid CMatrix provided.");
	check(i < mat->nr && j < mat->nc, "Index %zd,%zd out of bounds.", i, j);

	mat->data[i*mat->nc + j] = v;
	return;

error:
	return;
}


/** \brief CMatrix element updater
 *
 * A function which streamlines the update of a single element of a
 * CMatrix according to a function. That is, this, function applies
 * the passed function to the jth element in the ith row of mat.
 */
void
CMatrix_update(CMatrix * mat,		/**< [in] Matrix. */
	       const size_t i,		/**< [in] Row index. */
	       const size_t j,		/**< [in] Col index. */
	       double (*update_func)(double)	/**< [in] Function to update \f$mat_{i,j}\f$ with. */)
{
	CMatrix_set(mat, i, j, update_func(CMatrix_get(mat, i, j)));
}


/** \brief CVector allocator.
 */
CVector *
CVector_make(const size_t sz		/**< [in] Length of vector. */)
{
	CVector * v = (CVector *)CMatrix_make(sz, 1);
	return v;
}


/** \brief CVector copy constructor
 *
 * Return deep copy of parameter V.
 */
CVector *
CVector_copy(const CVector * V		/**< [in] CVector to copy. */)
{
	return (CVector *)CMatrix_copy((const CMatrix *)V);
}


/** \brief CVector freer.
 */
void
CVector_destroy(CVector * vec		/**< [in] Pointer to be freed. */)
{
	CMatrix_destroy((CMatrix *) vec);
}


/** \brief CVector getter.
 */
double
CVector_get(const CVector * vec,	/**< [in] Vector. */
	    const size_t idx		/**< [in] Index. */)
{
	return CMatrix_get((const CMatrix *) vec, idx, 1);
}


/** \brief CVector setter.
 */
void CVector_set(CVector * vec,		/**< [in] Vector. */
		 const size_t idx,		/**< [in] Index. */
		 const double val	/**< [in] New value. */)
{
	CMatrix_set((CMatrix *) vec, idx, 1, val);
}


/** \brief CVector updater
 * \sa CMatrix_update
 */
void
CVector_update(CVector * vec,		/**< [in] Vector. */
	       const size_t idx,		/**< [in] Index. */
	       double (*update_func)(double)	/**< [in] Function. */)
{
	CMatrix_update((CMatrix *) vec, idx, 1, update_func);
}


/** \brief SParams allocator
 */
SParams *
SParams_make()
{
	SParams * s = (SParams *)calloc(1, sizeof(SParams));
	check_mem(s);

	return s;

error:
	return NULL;
}


/** \brief SParams freer.
 */
void
SParams_destroy(SParams * surv		/**< [in] Pointer */)
{
	if (surv)
		free(surv);
}


/** \brief Default survey parameters
 */
void
SParams_defaults(SParams * surv		/**< [in] SParams object to initialize. */)
{
	check(surv, "Cannot initialize NULL SParams.");

	surv->dOmega = 12.0;
	return;

error:
	return;
}

/** \brief Binning allocator
 */
Binning *
Binning_make(const size_t nsegs,
	     double * bounds,
	     size_t * subdvs)
{
	Binning * b = (Binning *)malloc(sizeof(Binning));
	check_mem(b);

	b->nsegs = nsegs;

	b->bounds = (double *)malloc(sizeof(double) * (nsegs + 1));
	check_mem(b->bounds);
	memcpy(b->bounds, bounds, sizeof(double) * (nsegs + 1));

	b->subdvs = (size_t *)malloc(sizeof(size_t) * nsegs);
	check_mem(b->subdvs);
	memcpy(b->subdvs, subdvs, sizeof(size_t) * nsegs);

	return b;

error:
	Binning_destroy(b);
	return NULL;
}


/** \brief Binning freer
*/
void
Binning_destroy(Binning * b)
{
	if (b)
	{
		if (b->subdvs)
			free(b->subdvs);
		if (b->bounds)
			free(b->bounds);
		free(b);
	}
}

/** \brief Binning copier
 */
Binning *
Binning_copy(Binning * b)
{
	Binning * k = Binning_make(b->nsegs, b->bounds, b->subdvs);
	return k;
}
