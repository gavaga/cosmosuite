#ifndef TYPES_H
#define TYPES_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

/** \brief Configuration Integer
 *
 * Type used as a bit/nibble array for settings and
 * free parameters using the masks defined in constants.h
 */
typedef uint32_t conf_int;

/** \brief N-D Matrix structure
 *
 * N-D matrix structure for convenient accessors and setters
 */
typedef struct CTensor {
	size_t ndim;
	size_t * dims;
	double * data;
} CTensor;

/** \brief CMatrix
 *
 * Struct used internally to conveniently pass matrices around
 */
typedef struct CMatrix {
	size_t nr, nc;
	double * data;
} CMatrix;


/** \brief CVector
 *
 * Mask for CMatrix with one dimensions assumed to be of length 1, by default
 * assumed to be a vertical vector.
 */
typedef CMatrix CVector;


/** \brief Vector of Cosmological Parameters
 *
 * A struct used to keep track of the set of cosmological parameters
 */
typedef struct CParams
{
	double h0;
	double w;
	double wa;
	double omega_l_0;
	double omega_M_0;
	double omega_b_0;
	double omega_nu_0;
	double sigma_8;
	double ns;
	double gamma;
	double lambda_0;
	double sig_lambda;
	double beta_lambda;
} CParams;

 
/** \brief Vector of Survey Parameters
 *
 * Struct used to keep track of all survey parameters and other program 
 * options.
 */
typedef struct SParams
{
	double dOmega;
} SParams;

/** \brief Encapsulation of a binning entity
 *
 * Object used to pass around arbitrary Redshift and Tracer binnings.
 * The bounds array holds a list of bin section boundaries, the gaps
 * between which are divided into the number of bins listed in the
 * appropriate sudbvs array member. For example, bounds = [0, 10, 50],
 * subdvs = [5, 2] is equivalent to bounds = [0, 2, 4, 6, 7, 10, 30, 50],
 * subdvs = [1, 1, 1, 1, 1, 1, 1], as sudvs[0] = 5 tells us to subdivide the
 * [0, 10] bin into 5 even bins, and subdcs[1] = 2 tells us to subdivide the
 * [10, 50] bin into 2 even bins.
 */
typedef struct Binning
{
	size_t nsegs;
	double * bounds;
	size_t * subdvs;
} Binning;

CParams *
CParams_make();

void
CParams_destroy(CParams * theta);

CTensor *
CTensor_make(size_t ndim,
	     size_t* sizes);

CTensor *
CTensor_copy(const CTensor * T);

void
CTensor_destroy(CTensor * tens);

double
CTensor_get(const CTensor * T,
	    ...);

void
CTensor_set(CTensor * T,
	    double val,
	    ...);

void
CTensor_update(CTensor * T,
	       double (*update)(double),
	       ...);
	    

CMatrix *
CMatrix_make(size_t nr,
	     size_t nc);

CMatrix *
CMatrix_copy(const CMatrix * M);

void
CMatrix_destroy(CMatrix * m);

double
CMatrix_get(const CMatrix * m,
	    size_t i,
	    size_t j);

void
CMatrix_set(CMatrix * m,
	    size_t i,
	    size_t j,
	    double v);

void
CMatrix_update(CMatrix * m,
	       size_t i,
	       size_t j,
	       double (*update_func)(double));

CVector *
CVector_make(size_t sz);

CVector *
CVector_copy(const CVector * M);

void
CVector_destroy(CVector * m);

double
CVector_get(const CVector * vec,
	    size_t idx);

void
CVector_set(CVector * vec,
	    size_t idx,
	    double val);

void
CVector_update(CVector * vec,
	       size_t idx,
	       double (*update_func)(double));

SParams *
SParams_make();

void
SParams_destroy(SParams * surv);

void
SParams_defaults(SParams * surv);

Binning *
Binning_make(size_t nsegs,
	     double * bounds,
	     size_t * subdvs);

void
Binning_destroy(Binning * b);

Binning *
Binning_copy(Binning * b);

#endif /* TYPES_H */
