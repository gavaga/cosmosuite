/** \file utils.c
 * A collection of random utilities used
 * throughout the rest of the program.
 *
 * \author GMM
 * \date 2014-07-20
 */

#include "utils.h"

#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <math.h>
#include <sys/stat.h>
#include <stdio.h>

#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>

#include "debug.h"
#include "constants.h"
#include "fisher.h"
#include "survey.h"
#include "cosmology.h"


/** \brief Number of ones in binary
 *
 * Counts the number of ones in binary using constants
 * defined in constants.h and a tree-like addition
 * structure.
 */
size_t
popcount(uint64_t x				/**< [in] Number to analyze. */)
{
	x -= (x >> 1) & m1;
	x = (x & m2) + ((x >> 2) & m2);
	x = (x + (x >> 4)) & m4;
	return (size_t)((x * h01) >> 56);
}


/** \brief Counts the PARAMS bits set
 */
size_t
count_params(const conf_int s			/**< [in] Configuration flag array. */)
{
	return popcount(PARAMS(s));
}


/** \brief load_fiducial_params
 * A simple utility which loads and returns the 
 * fiducial parameter set.
 */
CVector *
load_fiducial_params(const conf_int s		/**< [in] Configuration parameters. */) 
{
	size_t nparams = count_params(s);
	CVector * params = CVector_make(nparams);
	check_mem(params);

	size_t idx = 0;
	for (size_t i = 0; i < TOTAL_PARAMS-1; ++i)
	{
		if (s & PFLAG(i))
		{
			CVector_set(params, i, get_fiducial((conf_int)PFLAG(i)));
			idx++;
		}
	}

	return params;

error:
	if (params)
		CVector_destroy(params);
	return NULL;
}


/**
 * Function which returns the fiducial parameter value from lcdm()
 * that is associated with the name given. Returns 0 if the parameter is not found.
 * \param name short name of the parameter to get, e.g. sl instead of sig_lambda
 */
double
get_fiducial(const conf_int ibit		/**< [in] Parameter bitmask. */)
{
	CParams * theta = lcdm();
	double rval;

	size_t index = (size_t)log2((double)(ibit >> PARAM_OFFSET));
	check(index < TOTAL_PARAMS, "Unknown parameter requested.");
	
	rval = (&(theta->h0))[index];

	CParams_destroy(theta);
	return rval;

error:
	if (theta)
		CParams_destroy(theta);
	return 0.0;
}


/** \brief Computes fiducial bins with dynamic binning
 */
CMatrix *
compute_fiducial_bins_dynamic(Binning ** binnings)
{
	check(binnings, "No binnings provided.");

	CParams * theta = lcdm();
	SParams * survey = wfirst();

	CMatrix * M = gen_N(theta, survey, binnings);

	CParams_destroy(theta);
	SParams_destroy(survey);

	return M;

error:
	return NULL;
}


/** \brief Loader for named experiment fiducial bins
 */
CMatrix *
load_fiducial_bins_named(char * name,			/**< [in] Name of experiment. */
			 Binning ** binnings		/**< [in] Binnings in Redshift, Richness, etc. */)
{
	FILE * fidfile = NULL;
	CMatrix * M = NULL;

	check(name, "No name provided.");

	size_t nz = calc_total_bins(binnings[0]);
	size_t nl = calc_total_bins(binnings[1]);

	char filename[255];
	sprintf(filename, "%s/%s.fid", DIR_VAR, name);

	struct stat buffer;
	int status = stat(filename, &buffer);

	if (status == 0 && (size_t)buffer.st_size == sizeof(CMatrix) + sizeof(double)*nz*nl)
	{
		M = (CMatrix *)malloc(sizeof(CMatrix));
		check_mem(M);

		fidfile = fopen(filename, "r");
		check(fidfile, "Failed to open %s.\n", filename);

		size_t read = fread(M, sizeof(CMatrix), 1, fidfile);
		check(read == 1, "Failed to read from %s.\n", filename);

		M->data = (double *)malloc(sizeof(double) * nz * nl);
		check_mem(M->data);
		read = fread(M->data, sizeof(double), nz * nl, fidfile);
		check(read == nz * nl, "Failed to read from %s.\n", filename);

		fclose(fidfile);
	}
	else
	{
		M = compute_fiducial_bins_dynamic(binnings);

		fidfile = fopen(filename, "w");
		check(fidfile, "Failed to open %s.\n", filename);

		size_t written = fwrite(M, sizeof(CMatrix), 1, fidfile);
		check(written == 1, "Failed to write file %s.\n", filename);

		written = fwrite(M->data, sizeof(double), nz * nl, fidfile);
		check(written == nz * nl, "Failed to write file %s.\n", filename);

		fclose(fidfile);
	}

	return M;

error:
	if (fidfile)
		fclose(fidfile);
	if (M)
		CMatrix_destroy(M);
	return NULL;
}


/** \brief Eigenvector/-value decomposition
 *
 * This function is a convenience wrapper for GSL's
 * eigen_symmv routine, which destructively computes the 
 * eigenvectors and eigenvalues of a symmetric matrix.
 */
void
eigen_decomposition(CMatrix * fisher,		/**< [in] Symmetric matrix, usually Fisher Information. */
		    CMatrix * evc,		/**< [out] Matrix with eigenvectors as columns. */
		    CVector * evl		/**< [out] Orthonormal vector of eigenvalues. */)
{
	check(fisher, "No valid fisher matrix was provided.");
	check(evc, "No valid eigenvector return pointer was provided.");
	check(evl, "No valid eigenvalue return pointer was provided.");

	gsl_matrix_view fish = gsl_matrix_view_array(fisher->data, fisher->nr, fisher->nc);
	gsl_matrix_view gevc = gsl_matrix_view_array(evc->data, evc->nr, evc->nc);
	gsl_vector_view gevl = gsl_vector_view_array(evl->data, evl->nr);

	gsl_eigen_symmv_workspace * w = gsl_eigen_symmv_alloc(fisher->nr);
	check_mem(w);

	gsl_eigen_symmv(&fish.matrix, &gevl.vector, &gevc.matrix, w);

	gsl_eigen_symmv_free(w);
	
error:
	return;
}


/** \brief Loads Eigen decomposition
 *
 * This is a function that attempts to load a saved pair of eigenvectors and eigenvalues 
 * associate with the given configuration \f$s\f$ from
 * the autoconf-defined var directory. If either of evc or evl are NULL, only the non-NULL
 * parameter will be returned. This function now combines the two previous functions
 * load_eigenvalues and load_eigenvectors.
 *
 * If the either of the files does not exist, the fisher information is loaded and the
 * pair are recomputed and saved in the appropriate folders.
 */
void
load_eigen(char * name,				/**< [in] Experiment reference name. */
	   const conf_int s,			/**< [in] Configuration settings. */
	   Binning ** binnings,			/**< [in] Dynamic binning structures. */
	   CMatrix * evc,			/**< [out] Matrix with eigenvectors as columns, if non-null. */
	   CVector * evl			/**< [out] Orthonormal vector of eigenvalues, if non-null. */)
{
	FILE  *fevc = NULL, *fevl = NULL;
	CMatrix * fisher = NULL;
	CMatrix * tevc = NULL;
	CVector * tevl = NULL;

	check(binnings, "No binnings provided.");
	check(evc != NULL || evl != NULL, "At least one of evc, evl must be non-null.");

	size_t nparams = count_params(s);

	char fnevc[255];
	char fnevl[255];
	sprintf(fnevc, "%s/%s.evc", DIR_VAR, name);
	sprintf(fnevl, "%s/%s.evl", DIR_VAR, name);

	struct stat buffer;
	int evc_status = stat(fnevc, &buffer);
	int evl_status = stat(fnevl, &buffer);

	if (evc_status == 0 && evl_status == 0)
	{
		debug("Eigen decomposition found, loading %s, %s.", fnevc, fnevl);

		if (evc != NULL)
		{
			fevc = fopen(fnevc, "r");

			size_t read = fread(evc, sizeof(CMatrix), 1, fevc);
			check(read == 1, "Could not read %s. Please purge this file and try again.", fnevc);
			
			if (!evc->data)
			{
				evc->data = (double *)malloc(nparams * nparams * sizeof(double));
				check_mem(evc->data);
			}

			read = fread(evc->data, sizeof(double), nparams* nparams, fevc);
			check(read == nparams*nparams, "Could not read %s. Please purge this file and try again.", fnevc);

			fclose(fevc);
		}

		if (evl != NULL)
		{
			fevl = fopen(fnevl, "r");

			size_t read = fread(evl, sizeof(CMatrix), 1, fevl);
			check(read == 1, "Could not read %s. Please purge this file and try again.", fnevl);

			if (!evc->data)
			{
				evl->data = (double *)malloc(nparams * sizeof(double));
				check_mem(evl->data);
			}

			read = fread(evl->data, sizeof(double), nparams, fevl);
			check(read == nparams, "Could not read %s. Please purge this file and try again.", fnevl);

			fclose(fevl);
		}
	}	
	else
	{
		debug("Eigen decomposition not found, computing.");

		tevc = evc;
		tevl = evl;

		if (tevc == NULL)
			tevc = CMatrix_make(nparams, nparams);
		if (tevl == NULL)
			tevl = CVector_make(nparams);

		fisher = load_fisher(name, s, binnings);
		check(fisher, "Could not compute fisher matrix.");

		eigen_decomposition(fisher, evc, evl); 

		CMatrix_destroy(fisher);
		fisher = NULL;

		fevc = fopen(fnevc, "w");
		fevl = fopen(fnevl, "w");

		size_t written = fwrite(evc, sizeof(CMatrix), 1, fevc);
		check(written == 1, "Could not write %s.", fnevc);

		written = fwrite(evc->data, sizeof(double), nparams * nparams, fevc);
		check(written == nparams*nparams, "Could not write %s.", fnevc);

		written = fwrite(evl, sizeof(CVector), 1, fevl);
		check(written == 1, "Could not write %s.", fnevl);

		written = fwrite(evl->data, sizeof(double), nparams, fevl);
		check(written == nparams, "Could not write %s.", fnevl);

		fclose(fevc);
		fclose(fevl);

		if (evc == NULL)
			CMatrix_destroy(tevc);
		if (evl == NULL)
			CVector_destroy(tevl);
	}

	return;
	
error:
	if (fevc)
		fclose(fevc);
	if (fevl)
		fclose(fevl);
	if (fisher)
		CMatrix_destroy(fisher);
	if (!evc && tevc)
		CMatrix_destroy(tevc);
	if (!evl && tevl)
		CVector_destroy(tevl);

	evc = NULL;
	evl = NULL;
	return;
}


/** \brief Loads Fisher Information
 *
 * This is a function that will attempt to load the file 
 * containing the Fisher Information associated with s.
 * If this succeeds, it will return
 * the contained matrix. If this fails, it will calculate the requested matrix, 
 * save it to that file, and return it.
 */
CMatrix *
load_fisher(char * name,			/**< [in] Reference name. */
	    const conf_int s,			/**< [in] Configuration settings. */
	    Binning ** binnings			/**< [in] Set of dynamic binnings. */)
{
	FILE * ffish = NULL;

	size_t nparams = count_params(s);

	char fnfish[255];
	sprintf(fnfish, "%s/%s.fsh", DIR_VAR, name);

	CMatrix * fish;

	struct stat buffer;
	int status = stat(fnfish, &buffer);

	if (status == 0 && (~s) & FLAG_FORCE)
	{
		debug("Fisher Information file found; loading %s.", fnfish);

		fish = CMatrix_make(nparams, nparams);
		check_mem(fish);
		
		ffish = fopen(fnfish, "r");

		size_t read = fread(fish, sizeof(CMatrix), 1, ffish);
		check(read == 1, "Could not read %s. Please purge this file and try again.", fnfish);

		read = fread(fish->data, sizeof(double), nparams*nparams, ffish);
		check(read == nparams*nparams, "Could not read %s. Please purge this file and try again.", fnfish);

		fclose(ffish);
	}
	else
	{
		debug("Fisher Information file %s not found; computing.", fnfish);
		fish = fisher_chain_rule(name, s, binnings);

		ffish = fopen(fnfish, "w");

		size_t written = fwrite(fish, sizeof(CMatrix), 1, ffish);
		check(written == 1, "Could not write %s.", fnfish);

		written = fwrite(fish->data, sizeof(double), fish->nr * fish->nc, ffish);
		check(written == fish->nr*fish->nr, "Could not write %s.", fnfish);

		fclose(ffish);
	}
	return fish;

error:
	if (ffish)
		fclose(ffish);
	if (fish)
		CMatrix_destroy(fish);
	return NULL;
}


/**
 * Utility for convenience that normalizes a raw parameter set from CosmoMC
 * by swapping the signs of w and wa back to normal, as well as subtracting 1
 * from beta_lambda, if any of these are present in the set. Other cases can 
 * easily be added to this, if it is decided to free other parameters which make
 * the computation of Fisher Information and subsequent eigendecomposition inconvenient.
 */
void
normalize_params(char * name,			/**< [in] Experiment reference name. */
		 CVector * const params,		/**< [in] Vector of parameter values. */
		 conf_int * s,					/**< [in] Configuration settings. */
		 Binning ** binnings			/**< [in] Set of dynamic binnings. */)
{
	CMatrix * evc = NULL;
	check(params, "No parameter vector was supplied.");
	check(s, "No configuration was supplied.");

	size_t nparams = count_params(*s);
	if (*s & FLAG_ROTATED)
	{
		evc = CMatrix_make(nparams, nparams);
		load_eigen(name, *s, binnings, evc, NULL);

		CMatrix_prod_inplace(evc, params, params);
		CMatrix_destroy(evc);

		/* Safeguard against double calling this function. */
		*s ^= FLAG_ROTATED;
	}

	if (*s & FLAG_LOG)
	{
		for (size_t i = 0; i < nparams; ++i)
		{
			CVector_update(params, i, exp);
		}

		*s ^= FLAG_LOG;
	}
	
	if (!(*s & FLAG_NORMALIZED))
	{
		for (size_t idx = 0; idx < TOTAL_PARAMS-1; ++idx) 
		{
			if (*s & PFLAG(idx))
			{
				if (PFLAG(idx) & (FLAG_W | FLAG_WA))
				{
					CVector_set(params, idx, -CVector_get(params, idx));
				}
				else if (PFLAG(idx) & FLAG_BL)
				{
					CVector_set(params, idx, CVector_get(params, idx) - 1);
				}
			}
		}

		*s ^= FLAG_NORMALIZED;
	}

	return;

error:
	if (evc)
		CMatrix_destroy(evc);
	return;
}


/** \brief Setup parameters (undo normalize)
 */
void
setup_params(char * name,				/**< [in] Experiment Reference Name. */
			 CVector * const params,	/**< [in] Vector of normalized parameters. */
			 conf_int *s,				/**< [in] Parameters key and settings. */
			 Binning ** binnings,		/**< [in] Dynamic binnings. Can be NULL if not rotating. */
			 bool blog,					/**< [in] Use log parameters? */
			 bool brotate				/**< [in] Rotate with eigenvectors? */)
{
	size_t nparams = count_params(*s);

	if (*s & FLAG_NORMALIZED)
	{
		for (size_t idx = 0; idx < TOTAL_PARAMS; ++idx)
		{
			if (*s & PFLAG(idx))
			{
				if (PFLAG(idx) & (FLAG_W|FLAG_WA))
				{
					CVector_set(params, idx, -CVector_get(params, idx));
				}
				else if (PFLAG(idx) & FLAG_BL)
				{
					CVector_set(params, idx, CVector_get(params, idx) - 1);
				}
			}
		}
		*s ^= FLAG_NORMALIZED;
	}

	if (blog)
	{
		for (size_t i = 0; i < nparams; ++i)
		{
			CVector_update(params, i, log);
		}

		*s ^= FLAG_LOG;
	}

	if (brotate)
	{
		CMatrix * evc = CMatrix_make(nparams, nparams);
		load_eigen(name, *s, binnings, evc, NULL);

		CMatrix_prod_inplace(evc, params, params);
		CMatrix_destroy(evc);
		
		*s ^= FLAG_ROTATED;
	}

	return;
}


/** \brief CMatrix Multiplication
 *
 * Multiplies matrices A and B using GSL's dgemm algorithm, saving the result to
 * C. If C is of improper dimension or A and B cannot be multiplied, no action is taken.
 */
void
CMatrix_prod_inplace(const CMatrix * A,			/**< [in] First operand. */
		     const CMatrix * B,			/**< [in] Second operand. */
		     CMatrix * R			/**< [out] Resultant Matrix. */)
{
	check(A->nc == B->nr && A->nr != R->nr && B->nc != R->nc, "Matrix dimensions do not match.");

	double alpha = 1.0;
	double beta = 0.0;

	gsl_matrix_view MA = gsl_matrix_view_array(A->data, A->nr, A->nc);
	gsl_matrix_view MB = gsl_matrix_view_array(B->data, B->nr, B->nc);
	gsl_matrix_view MR = gsl_matrix_view_array(R->data, R->nr, R->nc);

	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
		       alpha, &MA.matrix, &MB.matrix, beta, &MR.matrix);
	return;

error:
	return;
}


/** \brief CMatrix Multiplication
 *
 * Same as CMatrix_prod_inplace, but if A and B can be multiplied, it creates a matrix C of appropriate dimension
 * and returns a pointer to it. Otherwise, returns NULL.
 */
CMatrix *
CMatrix_prod(const CMatrix * A,				/**< [in] First operand. */
	     const CMatrix * B				/**< [in] Second operand. */)
{
	check(A->nc == B->nr, "Matrix inner dimensions do not match.");

	CMatrix * R = CMatrix_make(A->nr, B->nc);

	CMatrix_prod_inplace(A, B, R);

	return R;

error:
	return NULL;
}


/** \brief CMatrix printer
 *
 * Function which prints M in a convenient format.
 */
void
CMatrix_print(const CMatrix * M				/**< [in] Matrix to be printed. */)
{
	check(M, "No matrix was provided.");
	for (size_t i = 0; i < M->nr; ++i)
	{
		for (size_t j = 0; j < M->nc; ++j)
		{
			printf("%8.6e ", CMatrix_get(M, i, j));
		}
		printf("\n");
	}
	return;

error:
	return;
}


/** \brief Save a config file
 *
 * Save argc and argv to be used later
 */
void
save_commandline_args(char * fncfg,			/**< [in] Filename to save to. */
	    	      int ac,				/**< [in] Number of arguments in av. */
	    	      char ** av			/**< [in] Arguments to save. */)
{
	FILE * fcfg = fopen(fncfg, "w");
	check(fcfg, "Could not open %s for writing.", fncfg);

	for (size_t i = 0; i < (size_t)ac; ++i)
	{
		fprintf(fcfg, "%s\n", av[i]);
	}

	fclose(fcfg);
	return;

error:
	if (fcfg)
		fclose(fcfg);
	return;
}


/** \brief Load a config file
 *
 * Given a config filename, produce the argc and argv used to produce it.
 * \return Arguments read.
 */
int
load_commandline_args(char * fncfg,			/**< [in] Filename to read. */
	    	      char *** av			/**< [out] Arguments. */)
{
	FILE * fcfg = NULL;
	int ac = 0;
	char buffer[255];
	size_t lines_allocated = 16;

	char ** nav = (char **)calloc(lines_allocated, sizeof(char*));
	check_mem(nav);

	fcfg = fopen(fncfg, "r");
	check(fcfg, "Could not open %s for reading.", fncfg);

	char * endptr;
	while((fgets(buffer, 255, fcfg)))
	{
		if ((endptr = strchr(buffer, '\n')))
			*endptr = '\0';
		nav[ac] = (char *)calloc(strlen(buffer), sizeof(char));
		check_mem(nav[ac]);

		memcpy(nav[ac], buffer, strlen(buffer));
		memset(buffer, 0, 255);

		++ac;

		if ((size_t)ac >= lines_allocated)
		{
			size_t new_size = lines_allocated*2;

			nav = (char **)realloc(av, sizeof(char*)*new_size);
			check_mem(nav);

			lines_allocated = new_size;
		}
	}

	*av = nav;
	return ac;

error:
	if (fcfg)
		fclose(fcfg);
	if (av)
		free(av);
	return 0;
}


/** \brief Save a set of dynamic binnings
 */
void
save_binnings(char * name,			/**< [in] Experiment reference name. */
	      const size_t nbinnings,		/**< [in] Length of binnings array. */
	      Binning ** binnings		/**< [in] Array of dynamic binnings. */)
{
	char filename[255];
	sprintf(filename, "%s/%s.bin", DIR_VAR, name);

	FILE * binfile = fopen(filename, "w");
	check(binfile, "Couldn't open %s for writing.\n", filename);

	for (size_t i = 0; i < nbinnings; ++i)
	{
		size_t written = fwrite(binnings[i], sizeof(Binning), 1, binfile);
		check(written == 1, "Failed to write to %s.\n", filename);

		written = fwrite(binnings[i]->bounds, sizeof(double), binnings[i]->nsegs + 1, binfile);
		check(written == (binnings[i]->nsegs + 1), "Failed to write to %s.\n", filename);

		written = fwrite(binnings[i]->subdvs, sizeof(size_t), binnings[i]->nsegs, binfile);
		check(written == binnings[i]->nsegs, "Failed to write to %s.\n", filename);
	}

error:
	if (binfile)
		fclose(binfile);
}


/** \brief Load a set of dynamic binnings
 */
Binning **
load_binnings(char * name,			/**< [in] Experiment reference name to load. */
	      const size_t nbinnings		/**< [in] Number of binnings to read from file. */)
{
	Binning ** binnings = (Binning **)malloc(nbinnings * sizeof(Binning *));
	check_mem(binnings);
	
	char filename[255];
	sprintf(filename, "%s/%s.bin", DIR_VAR, name);

	FILE * binfile = fopen(filename, "r");
	check(binfile, "Could not open %s for reading.\n", filename);

	for (size_t i = 0; i < nbinnings; ++i)
	{
		binnings[i] = (Binning *)malloc(sizeof(Binning));
		check_mem(binnings[i]);

		size_t read = fread(binnings[i], sizeof(Binning), 1, binfile);
		check(read == 1, "Failed to read from %s.\n", filename);

		binnings[i]->bounds = (double *)malloc(sizeof(double) * (binnings[i]->nsegs + 1));
		check_mem(binnings[i]->bounds);

		read = fread(binnings[i]->bounds, sizeof(double), binnings[i]->nsegs + 1, binfile);
		check(read == (binnings[i]->nsegs + 1), "Failed to read from %s.\n", filename);

		binnings[i]->subdvs = (size_t *)malloc(sizeof(size_t) * binnings[i]->nsegs);
		check_mem(binnings[i]->subdvs);

		read = fread(binnings[i]->subdvs, sizeof(size_t), binnings[i]->nsegs, binfile);
		check(read == binnings[i]->nsegs, "Failed to read from %s.\n", filename);
	}

	return binnings;

error:
	if (binnings)
	{
		for (size_t i = 0; i < nbinnings; ++i)
		{
			Binning_destroy(binnings[i]);
		}
		free(binnings);
	}

	return NULL;
}


/** \brief Utility that prints a list of binnings
 */
void
print_binnings(const size_t nbinnings,		/**< [in] Length of the list. */
	       Binning ** binnings		/**< [in] List of binnings. */)
{
	check(nbinnings > 0, "No binnings to print.");
	check(binnings, "No binnings to print.");

	for (size_t i = 0; i < nbinnings; ++i)
	{
		printf("Binning %zd:\n\tnsegs: %zd\n", i, binnings[i]->nsegs);

		printf("\tbounds: [");
		for (size_t j = 0; j < binnings[i]->nsegs + 1; ++j)
		{
			printf("%8.2f", binnings[i]->bounds[j]);
		}
		printf("]\n");

		printf("\tsubdvs: [");
		for (size_t j = 0; j < binnings[i]->nsegs; ++j)
		{
			printf("%8zd", binnings[i]->subdvs[j]);
		}
		printf("]\n\n");
	}

	return;

error:
	return;
}

/** \brief Calculate total number of bins
 */
size_t
calc_total_bins(Binning * b			/**< [in] Dynamic binning entity. */)
{
	size_t n = 0;
	for (size_t i = 0; i < b->nsegs; ++i)
	{
		n += b->subdvs[i];
	}
	return n;
}


/** \brief Generate Cosmological Parameter Set
 *
 * This function takes a list of parameter values and a conf_int key
 * and uses them to produce a properly filled-out CParams struct
 * filling in with defaults any parameters not considered "free" by
 * presence in the paramlist and its key. Expects unnormalized parameters,
 * but will behave if the parameters are already normalized.
 */
CParams *
generate_cparams(char * name,			/**< [in] Experiment reference name. */
		 double * params_raw,		/**< [in] Raw free parameter array. */
		 conf_int key,			/**< [in] Key to which parameters are passed, and in what state. */
		 Binning ** binnings		/**< [in] Dynamic binnings. */)
{
	size_t nparams = popcount(PARAMS(key));
	CVector * params = CVector_make(nparams);
	memcpy(params->data, params_raw, sizeof(double) * nparams);

	normalize_params(name, params, &key, binnings);

	CParams * theta = lcdm();

	double * cparams = &theta->h0;

	size_t index = 0;
	for (size_t i = 0; i < TOTAL_PARAMS; ++i)
	{
		if (key & PFLAG(i))
		{
			cparams[i] = CVector_get(params, index);
			++index;
		}
	}

	return theta;
}


/** \brief Generate Set of Survey Parameters
 *
 * Puts the raw list of survey parameters in their
 * proper places in the SParams struct.
 */
SParams *
generate_sparams(double * params_raw		/**< [in] Survey parameters passed from CosmoMC. */)
{
	check(params_raw, "No raw parameter list provided.");

	SParams * surv = SParams_make();
	check_mem(surv);

	memcpy(surv, params_raw, sizeof(SParams));
	return surv;

error:
	if (surv)
		SParams_destroy(surv);
	return NULL;
}
