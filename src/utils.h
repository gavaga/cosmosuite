#ifndef UTILS_H // include guard
#define UTILS_H

#include <stdint.h>
#include <stdbool.h>

#include "types.h"

size_t
popcount(uint64_t x);

size_t
count_params(conf_int s);

CVector *
load_fiducial_params(conf_int s);

double
get_fiducial(conf_int name);

CMatrix *
compute_fiducial_bins_dynamic(Binning ** binnings);

CMatrix *
load_fiducial_bins_named(char * name,
			 Binning ** binnings);

void
eigen_decomposition(CMatrix * fisher,
		    CMatrix * evc,
		    CVector * evl);

void
load_eigen(char * name,
	   conf_int s,
	   Binning ** binnings,
	   CMatrix * evc,
	   CVector * evl); 

CMatrix *
load_fisher(char * name,
	    conf_int s,
	    Binning ** binnings);

void
normalize_params(char * name,
		 CVector * params,
		 conf_int * s,
		 Binning ** binnings);

void
setup_params(char * name,
			 CVector * params,
			 conf_int *s,
			 Binning ** binnings,
			 bool log, bool rotate);

CMatrix *
CMatrix_prod(const CMatrix * A,
	     const CMatrix * B);

void
CMatrix_prod_inplace(const CMatrix * A,
		     const CMatrix * B,
		     CMatrix * C);

void
CMatrix_print(const CMatrix * M);

void
save_commandline_args(char * cfgfile,
	    	      int ac,
	    	      char ** av);

int
load_commandline_args(char * cfgfile,
	     	      char *** ac);

void
save_binnings(char * name,
	      size_t nbinnings,
	      Binning ** binnings);

Binning **
load_binnings(char * name,
	      size_t nbinnings);

void
print_binnings(size_t nbinnings,
	       Binning ** binnings);

size_t
calc_total_bins(Binning * b);

CParams *
generate_cparams(char * name,
		 double * params_raw,
		 conf_int key,
		 Binning ** binnings);

SParams *
generate_sparams(double * surv_params);

#endif // include guard
